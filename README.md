#  isbpFinder v4.2

Last update of this documentation: 21-02-2014

IsbpFinder - Version 4.2

## AUTHORS:

- Frederic CHOULET - INRA Clermont-Ferrand, France  
- Aurélien BERNARD - INRA Clermont-Ferrand, France  

## DESCRIPTION:

isbpFinder is a Perl script dedicated to design ISBP (Insertion Site Based Polymorphism) markers.
For details about ISBP markers, see Paux et al., Plant Biotechnology Journal 2010(8):196-210.

It allows the creation of features representing primers/amplicons covering the junction between 2 transposable elements (TEs)
in order to obtain a graphical view of the different markers designed by viewing the feartures through a sequence annotation
software such as "Artemis".

isbpFinder can generate output files in CSV, EMBL and GFF format containing TEs and amplicons/primers.

IsbpFinder is based on:
    - "RepeatMasker" program for TE detection  
    - "Primer3" program for primer design for TE junction amplification.  


## COPYRIGHT

Please, do not redistribute this program directly without contacting us first <frederic.choulet@inra.fr> .


## How to cite 

Please, cite this publication if you use our program:

Paux E., Faure S., Choulet F., Roger D., Gauthier V., Martinant J.P., Sourdille P., Balfourier F.,
Le Paslier M.C., Chauveau A., Cakir M., Gandon B. and Feuillet C. Insertion Site-Based Polymorphism markers open new
perspectives for genome saturation and marker-assisted selection in wheat. Plant Biotechnology Journal 2010 (8) 196-210.


## Contact

CHOULET Frederic (frederic.choulet [at] clermont.inra.fr)


## Prerequisites  

   1. Perl (5.8.9+):
      Available at http://www.perl.org/get.html
      -> Developed and tested with version 5.8.9 (On a cluster running CentOS 5.5) and 5.14.2 (Workstation running Ubuntu 13.04)

   2. BioPerl (1.6.x+):
      Available at http://www.bioperl.org/
      -> Developed and tested with 1.6.9
      /!\: too old versions of BioPerl will generate problems or cause unexepected behavior

   3. RepeatMasker (3.2.6+):
      -> Developed and tested with version 3.2.6 (On a cluster running CentOS 5.5) and 4.0.3 (Workstation running Ubuntu 13.04)
      This program is available at http://www.repeatmasker.org/RMDownload.html
      /!\: Cross_match engine for RepeatMasker must be installed too

   4. A library of Transposable Elements:
      IsbpFinder has been developed for Triticeae genomes and with the TREP database
      Available at http://wheat.pw.usda.gov/ITMI/Repeats/
      Obviously it will work with any FASTA files you could use as a repeat library

   5. Primer3 (2.3.6+)
      Available at http://primer3.sourceforge.net/releases.php
      -> Developed and tested with release 2.3.6

   6. Artemis (optional)
      You will need a annotation viewer software for selecting the markers you want to test experimentally.
      There are many. I personaly work with Artemis available at http://www.sanger.ac.uk/Software/Artemis/


## Installation 

\* : Mandatory step
\+ : Optional step

   1. Download IsbpFinder

   2. Unpack Distribution
      \* gunzip IsbpFinder_X.X.tar.gz
      \* tar xvf IsbpFinder_X.X.tar
      \* chmod +x IsbpFinder_X.X/bin/*.pl (to make it executable)
      \+ Create a symlink to isbpFinder.pl in your own bin directory
      \+ Define and export an "ISBPFINDER_ROOT" environment variable that point to isbpFinder installation folder

   3. Configure
      \+ Edit the default XML configuration file named "default.xml" (located in the conf directory)
	or
      \+ Create your own XML configuration file and use isbpFinder --config command line parameters to use it


## Runing IsbpFinder 

You can summon isbpFinder help with the following command:

```console
isbpFinder.pl --help
```

You can check isbpFinder version and last modification date with command:

```console
isbpFinder.pl --version
```

Here is the current isbpFinder Help section:

```
# VERSION:   4.2 (21 february 2014)
# AUTHORS:   Frederic Choulet, Aurélien Bernard

USAGE:
	isbpFinder.pl [--dorm | --rm RepeatMasker_file] [--lib Repeat_library] [--output_prefix outfilePrefixName] [OPTIONS] fasta_file1 fasta_file2 ...

EXAMPLES:

	isbpFinder.pl --dorm --lib /usr/local/db/TREP.fas --output_prefix MyOutputPrefix --format all  MyFastaFile1.fas MyFastaFile2.fas
	isbpFinder.pl --rm MyRmFile.out.xm --lib /usr/local/db/TREP.fas --output_prefix MyOutputPrefix --format all  MyFastaFile1.fas MyFastaFile2.fas

OPTIONS:

    Commons/Miscellaneous options:

	--directory/-d <string>: Name of isbpFinder execution directory (optional).
	                         If not specified, a default time-based name will be used

	--thread/-cpu <integer>: Number of thread to use for multithread tools (RepeatMasker/Blast)
	                         If not specified, the following default value will be used: 2

	--config/-c <path>: Path to a custom XML configuration file (optional)

	--help/-h: Print this help message

	--version/-v: Print version number and last modification date

	--debug/-d: Activate full debug mode


   Output related options:

	--format/-f <string>: Output format (optional). Possible values are [csv|embl|gff|all]
	                      If not specified, the following default value will be used: all

	--delimiter/-del <string>: Character to use to delimitate field in the csv output. Possible values are: [;|tab]
	                           If not specified, the following default value will be used: ;

	--output_prefix/-o <file>: Output files prefix name
	                           If not specified, a clean name derived from the input file and sequence names will be used


    RepeatMasker related options:

	--dorm: Run RepeatMasker for each FASTA sequence before the primer design
	        If not used, RepeatMasler will not be executed
	        * Incompatible with the --rm option *

	--rm <path>: Path to a precomputed RepeatMasker result file (standard output, not XM format)
	             * Incompatible with the --dorm option *

	--lib/-l <path>: Path to the RepeatMasker library (in FASTA format) to use (--dorm) / used (--rm)
	                 * Mandatory for both --dorm and --rm mode *

	--quick/-q <string>: Run RepeatMasker with the -q option (-q is used to speed up). Possible values are [y|n|yes|no|true|false]
	                     If not used, the -q option will not be used


    Primer3 related options:

	--min_amplicon_length/-min <integer>: Minimum primer product size / Amplicon length
	                                      If not specified, the following default value will be used: 100

	--max_amplicon_length/-max <integer>: Maximum primer product size / Amplicon length
	                                      If not specified, the following default value will be used: 300


    Filtering options for CSV output:

     Note: The following filters can be used to define a subset of ISBPs that will be written in an additional output file (the .filtered output file)
           To be written in the filtered output file an ISBP must pass every activated filters

	--filter_by_confidence/-fc <string>: Filter ISBPs based on the confidence level. Possible values are [high|medium|low]
	                                     Enter a list of space separated confidence level (Ex: -fc high medium) or use this option several times (Ex: -fc high -fc medium)
	                                     For example, "high medium" will allow ISBPs with a high or medium confidence level to be written in the filtered CSV output file
	                                     If not specified, the following default value will be used: high medium

	--filter_by_jonction_type/-fj <array>: Filter ISBPs based on their junction type
	                                       Enter a list of space separated junction type names (Ex: -fj Cacta__Cacta Gypsy__Gypsy)
	                                       or use this option several times (Ex: -fj Cacta__Cacta -fj Gypsy__Gypsy)
	                                       ISBPs with junction types listed with this parameter can be written in the filtered CSV output file
	                                       If not specified, there will be no filtering based on junction names

	--reject_uncomplete/-ruc <string>: Reject ISBPs based on the description of the TE predicted on each side of the junction. Possible values are [y|n|yes|no|true|false]
	                                   Do not write ISBPs without one of the following keyword (on either side) in the filtered CSV output file: "complete|consensus|MITE"
	                                   If not specified, this filter will be used

	--reject_identical/-ri <string>: Reject ISBPs when two identical TEs are predicted on each side of the junction. Possible values are [y|n|yes|no|true|false]
	                                 Do not write ISBPs like "Gypsy__Gypsy" or "CACTA__CACTA" (for example) in the filtered CSV output file
	                                 If not specified, this filter will be used

	--reject_unknown/-ru <string>: Reject ISBPs when one or both TE comes from an "Unknown" superfamily. Possible values are [y|n|yes|no|true|false]
	                               Do not write "Unknown__XXX", "XXX_Unknown" or "Unknown__Unknown" ISBPs in the filtered CSV output file
	                               If not specified, this filter will be used
```

## Runing IsbpFinder on a SGE cluster

To run isbpFinder.pl on a cluster/server/computer running Sun Grid Engine you can alternatively:

1. Use (bash) shell encapsulation

  - Create a shell script containing your isbpFinder.pl command line  
  - Launch the isbpFinder job with qsub:

```console
qsub -cwd -V /path/to/encapsulation_shell.sh
```

2. Change the interpreter of the qsub command

  - Launch the isbpFinder job with qsub and the Perl interpreter:

```console
qsub -S /path/to/perl -cwd -V /path/to/isbpFinder.pl [Your options] /path/to/Your_fasta_file
```

Important note:

- Always use ABSOLUTE path for every file added to your command lines when using qsub !  
- For both case (shell encapsulation or custom interpreter) we strongly recommand to the set the "ISBPFINDER_ROOT" environment variable described earlier in this README file.  
- qsub "-V" option is mandatory to transfer your PATH and other environment variables to the job  


## Output Example 

If you run isbFinder with the following command line:

```console
isbpFinder.pl --lib /My_Home_Directory/My_Databases/trep_plus_v2013_01.fsa --dorm --format EMBL /My_Home_Directory/My_Query_Sequences/Multifasta.fas
```
And if your multifasta file contains 2 sequences named:

  - Sequence_1
  - Sequence_2

Then uou will get the following file tree:

```
    - (dDir) isbpFinder_xxxxxxxx (where xxx is a time derived unique value)
	- (file) isbpFinder.log
	- (dir) Multifasta
	    - (dir) Sequence_1
		- (file) Multifasta_Sequence_1.filtered_isbp.embl
		- (file) Multifasta_Sequence_1.isbp.embl
		- (file) Multifasta_Sequence_1.repeat_masker.embl
	    - (dir) Sequence_1
		- (file) Multifasta_Sequence_2.filtered_isbp.embl
		- (file) Multifasta_Sequence_2.isbp.embl
		- (file) Multifasta_Sequence_2.repeat_masker.embl
```

Where:

+ Multifasta_Sequence_1.isbp.embl : EMBL file containing all the amplicons designed for Sequence_1 of file Multifasta.xxx

+ Multifasta_Sequence_1.filtered_isbp.embl : EMBL file containing a subset of the amplicons designed for Sequence_1 of file Multifasta.xxx (Amplicons that had passed all default/custom filters)

+ Multifasta_Sequence_1.repeat_masker.embl : EMBL file containing RepeatMasker results for Sequence_1 of file Multifasta.xxx

You should open the 2 EMBL files simultaneously under Artemis to obtain a graphic view of all the ISBPs designed:  
This is the command line for superimposing files under Artemis:  

```console
/path/to/your/artemis/executable/artemis  Multifasta_Sequence_1.repeat_masker.embl + Multifasta_Sequence_1.isbp.embl
```

* Under Artemis: use the "One_Line_Per_Entry" option for a suitable view of the superimposed Features.

* The TEs for which an extremity has been detected are representing is light blue whereas those for which only an internal part has been detected are represented in blue.


## FAQ

Why the presence of a TE "extremity" is important ?

     For a single transposable element (TE) present on your sequence, RepeatMasker can detect several matches
     with the same or with different homologous elements present in your library.
     Thus, IsbpFinder will design ISBP markers at each end of matching regions and, thus, for several "junctions"
     that are not real junctions but are artificially due to a problem of TE identification.
     So, in order to help choosing suitable ISBPs which will really be locus-specific when runing the PCR,
     you can focus on TEs for which an extremity is predicted.

     This information is given by IsbpFinder in the tag /extremity_L=yes|no /extremity_R=yes|no
     The ISBP markers having "yes" value are the higher probability to be locus-specific.

     This information is also expressed as the CONFIDENCE index in the output file
     Probability can be "high", "medium" or "low".


Example:

```
	FT   Amplicon        9261..9525
	FT                   /ID="v443_2806_isbpFinder_9261_9525_Amplicon_001"
	FT                   /Right_element="ctgF_rep_0203 - RLG_Wilma_3B_108_J20-1,
	FT                   LTR_Gypsy, Wilma, complete"
	FT                   /Right_extremity="yes"
	FT                   /Note="Target_number: 2"
	FT                   /Note="Amplicon_size: 265"
	FT                   /Note="Amplicon_GC_percent: 40.38"
	FT                   /Note="Forward_primer: GAGAGAAGGCAAGGGCAGAG"
	FT                   /Note="Forward_primer_size: 20"
	FT                   /Note="Forward_TM: 60.108"
	FT                   /Note="Forward_stability: 3.3500"
	FT                   /Note="Reverse_primer: GCATTCCCCCTCTTCATGGT"
	FT                   /Note="Reverse_primer_size: 20"
	FT                   /Note="Reverse_TM: 59.740"
	FT                   /Note="Reverse_stability: 3.5500"
	FT                   /Left_extremity="no"
	FT                   /color="3"
	FT                   /Confidence_index="high"
	FT                   /Junction_type="copia__gypsy"
	FT                   /Ontology_term="SO:0000006"
	FT                   /Junction_position_on_PCR_product="133..134"
	FT                   /Junction_position_on_query_sequence="9393..9394"
	FT                   /Left_element="ctgD_rep_0318 - RLC_Barbara_3B_090_L01-1,
	FT                   LTR_Copia, Barbara, fragmented"
	FT                   /Name="isbpFinder_9261_9525_Amplicon_001"
```
     In this example, two primers were designed for amplifying the junction between a "copia" and a "gypsy"  
     The /Left_extremity and /Right_extremity tags indicate if the extremity of the left or right TE (copia and gypsy respectively) has been detected.  

     In this example, the extremity of the right element (gyspy) is detected which means that the copia inserted within the gypsy.

     When an extremity is detected, the ISBP is more likely to be locus-specific.  
     This is of a great importance when you want to select some ISBP to test experimentally.
