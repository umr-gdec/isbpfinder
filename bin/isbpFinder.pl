#!/usr/bin/env perl

=head copyright
CHOULET Frederic
INRA Clermont-Ferrand
INRA-UBP UMR 1095 Genetique, Diversite et Ecophysiologie des Cereales
5, Chemin de Beaulieu
63100 Clermont-Ferrand
FRANCE
=cut

=head release notes
AUTHORS:   Frederic Choulet, Aurélien Bernard
PURPOSE:   This script is used to design ISBP markers from RepeatMasker results
CREATED:   2007 June 11th
CONTACT:   frederic.choulet@clermont.inra.fr
=cut

# Prepare custom module's loading
BEGIN {
	# Load required modules for path construction
	use File::Spec::Functions qw(rel2abs);
	use File::Basename;
	use FindBin;

	our $isbpFinderlibFolder = '';

	# Use various way to get the absolute path to the isbpFinder library folder
	if (defined($ENV{"ISBPFINDER_ROOT"}) && -d Cwd::realpath($ENV{"ISBPFINDER_ROOT"}) . '/lib') {
		$isbpFinderlibFolder = Cwd::realpath($ENV{"ISBPFINDER_ROOT"}) . '/lib';
	} elsif (-d $FindBin::RealBin . '/../lib/') {
		$isbpFinderlibFolder = dirname($FindBin::RealBin) . '/lib';
	} else {
		my $realExecutablePath = (-l $0) ? readlink(rel2abs($0)) : rel2abs($0);
		$isbpFinderlibFolder = dirname(dirname($realExecutablePath)) . '/lib';
	}
};

use lib $isbpFinderlibFolder;

# Basic Perl modules
use strict;
use warnings;
use diagnostics;

# Perl modules
use Getopt::Long;
use Log::Log4perl qw(get_logger :levels);
use Switch;

# Perl debug modules
use Data::Dumper;

# BioPerl modules
use Bio::SeqIO;

# Custom modules
use Configuration_loader;
use RepeatMasker;
use Primer3;
use Feature_builders;
use Writers;
use Misc;

# Declare global variables
my ($help, $debugMode, $printVersion) = (undef)x3;
my %parameters = ( 'Execute_RepeatMasker' => 0, 'Do_clustering_only' => 0, 'Run_clustering' => 0, 'Confidence_filter_levels' => [], 'Junction_filter_list' => []);

# Times
my $systemStartTime = time();
my $realStartTime = Misc::humanReadableDate();

# Version
my $version = '4.2';
my $lastModificationDate = '23 june 2014';


##################################################
## Manage options
##################################################

&GetOptions ('format|f=s'						=> \$parameters{'Output_format'},
			'output_prefix|o=s'					=> \$parameters{'Output_prefix'},
			'delimiter|del=s'					=> \$parameters{'Field_delimiter'},
			'thread|cpu=i'						=> \$parameters{'Number_of_thread'},
			'dorm'								=> \$parameters{'Execute_RepeatMasker'},
			'quick|q=s'							=> \$parameters{'Use_quick_search'},
			'rm=s'								=> \$parameters{'Existing_Repeatmasker_file'},
			'lib|l=s'							=> \$parameters{'RepeatMasker_library'},
			'min_amplicon_length|min=i'			=> \$parameters{'Minimum_amplicon_length'},
			'max_amplicon_length|max=i'			=> \$parameters{'Maximum_amplicon_length'},
			'filter_by_confidence|fc=s{1,3}'	=> $parameters{'Confidence_filter_levels'},
			'filter_by_jonction_type|fj=s{,}'	=> $parameters{'Junction_filter_list'},
			'reject_uncomplete|ruc=s'			=> \$parameters{'Reject_uncomplete'},
			'reject_identical|ri=s'				=> \$parameters{'Reject_identical'},
			'reject_unknown|ru=s'				=> \$parameters{'Reject_unknown'},
			'config|c=s'						=> \$parameters{'Config_file'},
			'directory|d:s'						=> \$parameters{'Execution_directory'},
			'version|v'							=> \$printVersion,
			'help|h'							=> \$help,
			'debug'								=> \$debugMode
			);

# Convert every relative path into absolute path to avoid problems afterward
$parameters{'Existing_Repeatmasker_file'} = Cwd::realpath($parameters{'Existing_Repeatmasker_file'}) if (defined($parameters{'Existing_Repeatmasker_file'}));
$parameters{'Config_file'} = Cwd::realpath($parameters{'Config_file'}) if (defined($parameters{'Config_file'}));
$parameters{'RepeatMasker_library'} = Cwd::realpath($parameters{'RepeatMasker_library'}) if (defined($parameters{'RepeatMasker_library'}));
for my $indexARGV (0 .. $#ARGV) { $ARGV[$indexARGV] = Cwd::realpath($ARGV[$indexARGV]); }

# Create the execution directory
my $executionDirectory = (defined($parameters{'Execution_directory'})) ? $parameters{'Execution_directory'} : 'isbpFinder_' . $systemStartTime;
Misc::jumpIn($executionDirectory) if (!$help && !$printVersion && scalar(@ARGV) > 0);

# Initialize Logger
my $mainLogger = createLogger($debugMode, $help, $printVersion, scalar(@ARGV));

# Load Configuration (default or custom)
# Note: Must be loaded here to avoid having hard coded default values in the code (even if we just have to display the help section)
my $xmlConfiguration = Configuration_loader::getConfiguration(\%parameters, $version);

# Display help message if needed
$printVersion and displayVersion($0, $version, $lastModificationDate);
$help and displayHelp($xmlConfiguration); # --help/-h option used
@ARGV or displayHelp($xmlConfiguration); # No argument


####################
###     MAIN     ###
####################

# Welcome message
my $diezShift = '#'x(length($version));
$mainLogger->info('###############################################################' . $diezShift);
$mainLogger->info('#             Welcome in isbpFinder.pl (Version ' . $version . ')             #');
$mainLogger->info('###############################################################' . $diezShift);
$mainLogger->info('Start time: ' . $realStartTime);
$mainLogger->info('');

# Check parameters
checkParameters(\%parameters, $xmlConfiguration);

# Initializations
my $repeatMaskerResults = {};

# Index the RepeatMasker library
my $rmLibraryIndex = RepeatMasker::indexRepeatMaskerLibrary($parameters{'RepeatMasker_library'});

# Parse the RepeatMasker result file given as command line argument if needed
if (defined($parameters{'Existing_Repeatmasker_file'})) {
	$mainLogger->info('');
	$mainLogger->info('Parsing of the existing RepeatMasker result file: ' . basename($parameters{'Existing_Repeatmasker_file'}));
	$repeatMaskerResults = RepeatMasker::parseRepeatMasker($xmlConfiguration->{'Transposable_elements'}, $parameters{'Existing_Repeatmasker_file'}, $xmlConfiguration->{'RepeatMasker'}, $rmLibraryIndex);
	$mainLogger->info('  -> This file contains results for ' . scalar(keys %{$repeatMaskerResults}) . ' sequence(s): ' . join(', ', keys %{$repeatMaskerResults}));
}

# Main loop - Browse input fasta files
foreach my $inputFastaFileFullPath ( @ARGV ) {
	# Initializations
	$repeatMaskerResults = {} if (!defined($repeatMaskerResults));

	# Get file basename and dirname
	my ($inputFastaFileBaseName, $inputFastaFilePath, $extension) = fileparse($inputFastaFileFullPath, qr/\.[^.]*/);

	$mainLogger->info('');
	$mainLogger->info('Treatment of file ' . $inputFastaFileBaseName . $extension . ':');

	# Check File existence
	Misc::checkFileExistence($inputFastaFileFullPath, 'Fasta');

	# Create a directory for the current fasta file analysis and jump in
	Misc::jumpIn($inputFastaFileBaseName);

	# Run RepeatMasker if requested (-dorm command line option)
	if ($parameters{'Execute_RepeatMasker'}) {
		$mainLogger->info('');
		$mainLogger->info('  - Execution of RepeatMasker with the following library: ' . basename($parameters{'RepeatMasker_library'}));
		RepeatMasker::executeRepeatMasker($xmlConfiguration->{'Bin'}->{'repeatMaskerExecutablePath'}, $parameters{'Number_of_thread'}, $parameters{'RepeatMasker_library'}, $parameters{'Use_quick_search'}, $inputFastaFileFullPath, $debugMode);
		$repeatMaskerResults = RepeatMasker::parseRepeatMasker($xmlConfiguration->{'Transposable_elements'}, 'RepeatMasker_results.out', $xmlConfiguration->{'RepeatMasker'}, $rmLibraryIndex);
	}

	# Browse the multi-fasta file
	my $fastaInputStream = Bio::SeqIO->new( '-file' => $inputFastaFileFullPath, '-format' => 'FASTA' );

	NEXTSEQ: while ( my $sequenceObject = $fastaInputStream->next_seq() ){
		# Clean sequence name and update the sequence object
		my $cleanSequenceID = $sequenceObject->display_id();
		$cleanSequenceID =~ s/[^\w#\.\-]/_/g;
		$sequenceObject->display_id($cleanSequenceID);

		# Initializations
		my $querySequenceName = $sequenceObject->display_id();
		my $filePrefix = '';
		my @rawAmpliconFeatureArray = ();
		my $repeatCounter = 0;

		# Create a directory for the current sequence analysis and jump in
		Misc::jumpIn($querySequenceName);

		$mainLogger->info('');
		$mainLogger->info('  - Analysis of the following sequence: ' . $querySequenceName);

		# Go to the next sequence if there is no result for the current sequence in Repeatmasker output file
		if (!defined($repeatMaskerResults->{$querySequenceName}) || scalar(@{$repeatMaskerResults->{$querySequenceName}}) == 0) {
			$mainLogger->info('     -> 0 repeat(s) available for this sequence !');
			chdir('..');
			next NEXTSEQ;
		} else {
			$mainLogger->info('     -> ' . scalar(@{$repeatMaskerResults->{$querySequenceName}}) . ' repeat(s) available for this sequence !');
		}

		# Adapt file prefix with the command line option -output_prefix/-o
		if (defined($parameters{'Output_prefix'})) {
			$filePrefix = $parameters{'Output_prefix'} . '_' . $querySequenceName;
		} else {
			$filePrefix = $inputFastaFileBaseName . '_' . $querySequenceName;
		}

		# Browse the list of repeats (sort on start position)
		for (my $index = 0; $index <= $#{$repeatMaskerResults->{$querySequenceName}}; $index++) {
			# Initializations
			my $currentRepeatFeature = $repeatMaskerResults->{$querySequenceName}->[$index];
			my $featureNoteData = Misc::getHashFromNote($currentRepeatFeature);

			$mainLogger->info('');
			$mainLogger->info('     -> Working with the following repeat: ' . $currentRepeatFeature->display_name());

			# Keep sequences for which it will be possible to design a primer (avoid junctions too close from the ends of the sequence)
			if ($currentRepeatFeature->start() <= 30 and $featureNoteData->{'Query_Unmasked'} >= -30) {
				$mainLogger->warn("\t" . '--> Warning: This repeat extends over the whole sequence !');
				$mainLogger->warn("\t" . '--> No ISBP could be designed..');
				chdir('..');
				next NEXTSEQ;
			}

			$repeatCounter += 1;

			# Collect data before Primer3 execution
			my $closeRepeatsData = RepeatMasker::getCloseRepeatsData($repeatMaskerResults, $querySequenceName, $index);
			my $targetsData = Primer3::buildTargets($currentRepeatFeature, $featureNoteData->{'Query_Unmasked'});

			TARGET: foreach my $currentTarget (@{$targetsData}) {
				# Jump to the next target if needed
				if ($currentTarget->{'position'} eq 'none') {
					$mainLogger->info("\t" . '-> Target n°' . $currentTarget->{'number'} . ' - No junction position defined');
					next;
				} else { $mainLogger->info(''); }

				# Initializations
				my $junction_position = $currentTarget->{'position'} + 10;
				my $primer3InputFile = $filePrefix . '_Repeat_' . $repeatCounter . '_target_' . $currentTarget->{'number'} . '.inpr3';
				my $primer3OutputFile = $filePrefix . '_Repeat_' . $repeatCounter . '_target_' . $currentTarget->{'number'} . '.outpr3';

				# Check if primers have already been designed for the current junction
				foreach my $existingAmpliconFeature (@rawAmpliconFeatureArray) {
					if ($junction_position >= $existingAmpliconFeature->start() and $junction_position <= $existingAmpliconFeature->end()) {
						$mainLogger->info("\t" . '-> Target n°' . $currentTarget->{'number'} . ' - Amplicon already designed for the current junction position (' . $junction_position . '). Jumping to next target !');
						next TARGET;
					}
				}

				$mainLogger->info("\t" . '-> Primer search for target n°' . $currentTarget->{'number'} . ' (Junction position: ' . $junction_position . ')');

				# Prepare Primer3 input file
				my $subSequenceData = Primer3::preparePrimer3SubSequence($sequenceObject, $currentTarget, $currentRepeatFeature, $closeRepeatsData, $parameters{'Maximum_amplicon_length'});
				Primer3::generatePrimer3InputFile($primer3InputFile, $subSequenceData, $currentTarget, $parameters{'Minimum_amplicon_length'}, $parameters{'Maximum_amplicon_length'}, $xmlConfiguration->{'Primer3'}->{'numberOfPrediction'});

				# Run PRIMER3
				my $primer3Command = $xmlConfiguration->{'Bin'}->{'primer3ExecutablePath'} . ' -output=' . $primer3OutputFile . ' ' . $primer3InputFile;
				$mainLogger->debug('Primer3 will be executed with the following command line: ' . $primer3Command);
				system ($primer3Command);

				# Parse Primer3 output file
				my $refToPrimerDatas = Primer3::parsePrimer3Output($primer3OutputFile, $subSequenceData->{'sequence'});
				#my $refToPrimerDatas = Primer3::parsePrimer3Output_obj($primer3OutputFile);

				# Remove Primer3 files if needed
				unlink ($primer3InputFile, $primer3OutputFile) if (!$debugMode);

				# Jump to next target/junction if needed
				if (scalar(@{$refToPrimerDatas}) == 0) {
					$mainLogger->info("\t" . '  --> No primer designed by Primer3');
					next;
				} else { $mainLogger->info("\t" . '  --> Search complete ! Building amplicon feature..'); }

				# Create amplicon features with Primer3 results
				foreach my $primerPair (@{$refToPrimerDatas}) {
					push(@rawAmpliconFeatureArray, Feature_builders::buildAmpliconFeature($primerPair, $querySequenceName, $subSequenceData->{'start'}, $currentTarget->{'number'}));
				}
			}
		}
		$mainLogger->info('');


		# Write all RepeatMasker features
		if ($repeatCounter > 0) {
			Writers::writeEMBL($filePrefix . '.repeat_masker.embl', $sequenceObject->seq(), $repeatMaskerResults->{$querySequenceName}, 'Repeat Masker') if ($parameters{'Output_format'} =~ /^(embl|all)$/i);
			Writers::writeGFF($filePrefix . '.repeat_masker.gff', $sequenceObject, $repeatMaskerResults->{$querySequenceName}, 'Repeat Masker', $version) if ($parameters{'Output_format'} =~ /^(gff|all)$/i);
		} else {
			$mainLogger->warn('     -> Warning: No valid repeat available for the current sequence..');
			$mainLogger->warn("\t" . '-> TE junction cannot be determinded !');
			chdir('..');
			next NEXTSEQ;
		}

		# Update (with junction data) and write all amplicon features
		my $numberOfAmplicon = scalar(@rawAmpliconFeatureArray);
		my @cleanAmpliconFeatureArray = ();

		if ($numberOfAmplicon > 0) {
			$mainLogger->info('');
			$mainLogger->info('     -> Collecting of additional data for the ' . $numberOfAmplicon . ' generated amplicons (Sequence: ' . $querySequenceName . '):');

			for (my $ampliconNumber = 0; $ampliconNumber < $numberOfAmplicon; $ampliconNumber++) {
				$mainLogger->info('');
				$mainLogger->info("\t" . '-> Working with amplicon n°' . ($ampliconNumber + 1));

				my $junctionData = collectJunctionData($rawAmpliconFeatureArray[$ampliconNumber], $repeatMaskerResults->{$querySequenceName});

				# Do not kept ISBP amplicon that are completely included into TEs
				if ($junctionData->{'Full_overlap'} eq 'yes') {
					$mainLogger->debug('Amplicon feature n°' . ($ampliconNumber+1) . ' (Array entry n°' . $ampliconNumber . ') will not be added to the final array of amplicon features !');
					next;
				} else {
					Feature_builders::updateAmpliconFeature($rawAmpliconFeatureArray[$ampliconNumber], $junctionData, $xmlConfiguration->{'Transposable_elements'}, $rmLibraryIndex);
					push(@cleanAmpliconFeatureArray, $rawAmpliconFeatureArray[$ampliconNumber]);
				}
			}

			# Get an array of filtered amplicon features
			my $filteredAmpliconFeatures = filterAmpliconFeatures(\%parameters, \@cleanAmpliconFeatureArray);

			# Write amplicon and filtered features in the desired format(s)
			if (scalar(@cleanAmpliconFeatureArray) > 0) {
				# Write all amplicon features
				switch ($parameters{'Output_format'}) {
					case /csv/i { Writers::writeTabular($filePrefix . '.isbp.csv', \@cleanAmpliconFeatureArray, 'amplicon', $parameters{'Field_delimiter'}); }
					case /embl/i { Writers::writeEMBL($filePrefix . '.isbp.embl', $sequenceObject->seq(), \@cleanAmpliconFeatureArray, 'amplicon'); }
					case /gff/i { Writers::writeGFF($filePrefix . '.isbp.gff', $sequenceObject, \@cleanAmpliconFeatureArray, 'amplicon', $version); }
					case /all/i { Writers::writeAll($filePrefix . '.isbp', $sequenceObject, \@cleanAmpliconFeatureArray, 'amplicon', $parameters{'Field_delimiter'}, $version); }
					else { $mainLogger->error('Error: There is no writing method available for the requested output format !'); }
				}

				# Write filtered amplicon features
				if (scalar(@{$filteredAmpliconFeatures}) > 0) {
					switch ($parameters{'Output_format'}) {
						case /csv/i { Writers::writeTabular($filePrefix . '.filtered_isbp.csv', $filteredAmpliconFeatures, 'filtered amplicon', $parameters{'Field_delimiter'}); }
						case /embl/i { Writers::writeEMBL($filePrefix . '.filtered_isbp.embl', $sequenceObject->seq(), $filteredAmpliconFeatures, 'filtered amplicon'); }
						case /gff/i { Writers::writeGFF($filePrefix . '.filtered_isbp.gff', $sequenceObject, $filteredAmpliconFeatures, 'filtered amplicon', $version); }
						case /all/i { Writers::writeAll($filePrefix . '.filtered_isbp', $sequenceObject, $filteredAmpliconFeatures, 'filtered amplicon', $parameters{'Field_delimiter'}, $version); }
						else { $mainLogger->error('Error: There is no writing method available for the requested output format !'); }
					}
				} else {
					$mainLogger->info('');
					$mainLogger->info('     -> There is no filtered amplicon feature to write for the current sequence..');
				}
			} else {
				$mainLogger->info('');
				$mainLogger->info('     -> There is no amplicon feature to write for the current sequence..');
			}
		} else {
			$mainLogger->info('');
			$mainLogger->info('     -> No raw amplicon generated (No Primer3 result)..');
		}

		$mainLogger->info('');
		$mainLogger->info('     => Analysis of sequence "' . $querySequenceName . '" complete !');
		$mainLogger->info('');

		# Go back to the analysis folder for the current fasta file
		chdir('..');
	}
	# Go back to the main execution directory
	chdir('..');
}

# End message
$mainLogger->info('');
$mainLogger->info('Stop time: ' . Misc::humanReadableDate());
$mainLogger->info('##################################################################');
$mainLogger->info('#                        End of execution                        #');
$mainLogger->info('##################################################################');


###################
### SUBROUTINES ###
###################

####################################
## RepeatMasker related methods
####################################

# Moved to module RepeatMasker.pm


###############################
## Primer3 related methods
###############################

# Moved to module Primer3.pm


############################################
## Feature creation related methods
############################################

# Moved to module Feature_builders.pm


################################################
## Junction data recovery related methods
################################################

sub collectJunctionData {

	# Retrieval of parameters
	my ($currentAmpliconFeature, $repeatFeatures) = @_;

	# Initializations
	my %junctionData = ('Left_element' => 'no_element', 'Left_extremity' => 'no', 'Right_element' => 'no_element', 'Right_extremity' => 'no', 'Full_overlap' => 'no');

	# Collect left and right element/repeat
	for (my $repeatNumber = 0; $repeatNumber <= $#{$repeatFeatures}; $repeatNumber++) {
		# Initializations
		my $currentRepeatFeature = $repeatFeatures->[$repeatNumber];
		my $featureNoteData = Misc::getHashFromNote($currentRepeatFeature);
		my ($repeatName, $repeatStart, $repeatEnd) = $currentRepeatFeature->get_tag_values('Target');

		# Tag ISBP amplicon that are completely included into TEs
		if ($currentAmpliconFeature->start() >= $currentRepeatFeature->start() and $currentAmpliconFeature->end() <= $currentRepeatFeature->end()) {
			$mainLogger->warn("\t" . '  --> Warning: Repeat "' . $currentRepeatFeature->display_name() . '" totally overlap current amplicon (' . $currentAmpliconFeature->start() . '..' . $currentAmpliconFeature->end() . ')');
			$mainLogger->warn("\t" . '  --> Amplicon will not be kept !');
			$junctionData{'Full_overlap'} = 'yes';
			last;
		}

		# Avoid unnecessary comparisons
		next if ($currentRepeatFeature->end() < $currentAmpliconFeature->start());
		last if ($currentRepeatFeature->start() > $currentAmpliconFeature->end());

		# Get the first left element/TE for which an extremity was clearly identified
		if ($junctionData{'Left_extremity'} ne 'yes') {
			if ($currentAmpliconFeature->start() >= $currentRepeatFeature->start() and $currentAmpliconFeature->start() <= $currentRepeatFeature->end()) {
				$junctionData{'Left_extremity'} = ($currentRepeatFeature->strand() == 1) ? $featureNoteData->{'End_at_extremity'} : $featureNoteData->{'Start_at_extremity'};
				$junctionData{'Left_element'} = $repeatName;
				$junctionData{'Left_element_start'} = $currentRepeatFeature->start();
				$junctionData{'Left_element_end'} = $currentRepeatFeature->end();
				next; # If the current repeat is on the left of the amplicon then it can be on the right
			}
		}

		# Get the first right element/TE for which an extremity was clearly identified
		if ($junctionData{'Right_extremity'} ne 'yes') {
			if ($currentAmpliconFeature->end() >= $currentRepeatFeature->start() and $currentAmpliconFeature->end() <= $currentRepeatFeature->end()) {
				$junctionData{'Right_extremity'} = ($currentRepeatFeature->strand() == 1) ? $featureNoteData->{'Start_at_extremity'} : $featureNoteData->{'End_at_extremity'};
				$junctionData{'Right_element'} = $repeatName;
				$junctionData{'Right_element_start'} = $currentRepeatFeature->start();
				$junctionData{'Right_element_end'} = $currentRepeatFeature->end();
			}
		}
	}

	# Determine the position of the junctions
	getJunctionPositions(\%junctionData, $currentAmpliconFeature) if ($junctionData{'Full_overlap'} eq 'no');

	$mainLogger->debug('Collected junction data:');
	$mainLogger->debug(Dumper(\%junctionData));

	return \%junctionData;
}


sub getJunctionPositions {

	# Retrieval of parameters
	my ($junctionData, $currentAmpliconFeature) = @_;

	# Calculate the junction position on the PCR product and the query sequence
	if ($junctionData->{'Left_element'} ne 'no_element') {
		$junctionData->{'Left_position_on_query'} = sprintf("%.0f..%.0f", $junctionData->{'Left_element_end'}, $junctionData->{'Left_element_end'} + 1);
		$junctionData->{'Left_position_on_PCR_product'} = sprintf("%.0f..%.0f", ($junctionData->{'Left_element_end'} - $currentAmpliconFeature->start() + 1), ($junctionData->{'Left_element_end'} - $currentAmpliconFeature->start() + 2));
	}

	if ($junctionData->{'Right_element'} ne 'no_element') {
		$junctionData->{'Right_position_on_query'} = sprintf("%.0f..%.0f", $junctionData->{'Right_element_start'} - 1, $junctionData->{'Right_element_start'});
		$junctionData->{'Right_position_on_PCR_product'} = sprintf("%.0f..%.0f", ($junctionData->{'Right_element_start'} - $currentAmpliconFeature->start()), ($junctionData->{'Right_element_start'} - $currentAmpliconFeature->start() + 1));
	}

	return 0; # SUCCESS
}


###################################################
## Amplicon features filtering related methods
###################################################

sub filterAmpliconFeatures {

	# Retrieval of parameters
	my ($cmdLineParameters, $seqFeaturesArrayRef) = @_;

	# Initializations
	my @filteredFeatures = ();

	# Browse the list of raw features
	foreach my $currentFeature (@{$seqFeaturesArrayRef}) {
		# Some variable initializations (Not really needed but the syntax is clearer with them)
		my $ampliconID = join(',', $currentFeature->get_tag_values('ID'));
		my $junctionType = ($currentFeature->has_tag('Junction_type')) ? join(',', $currentFeature->get_tag_values('Junction_type')) : '.';
		my $leftElement = ($currentFeature->has_tag('Left_element')) ? join(',', $currentFeature->get_tag_values('Left_element')) : '.';
		my $leftExtremity = ($currentFeature->has_tag('Left_extremity')) ? join(',', $currentFeature->get_tag_values('Left_extremity')) : '.';
		my $rightElement = ($currentFeature->has_tag('Right_element')) ? join(',', $currentFeature->get_tag_values('Right_element')) : '.';
		my $rightExtremity = ($currentFeature->has_tag('Right_extremity')) ? join(',', $currentFeature->get_tag_values('Right_extremity')) : '.';

		# Determine the confidence index of the amplicon based on it's junction type and extremity tags
		my $computedConfidenceIndex = determineConfidenceIndex($junctionType, $leftExtremity, $rightExtremity);
		$currentFeature->add_tag_value('Confidence_index', $computedConfidenceIndex);

		# Flag/Mark ISBPs that must be filtered out (0 = not written in filtered file, 1 = written in full AND filtered file)
		my $keepAmplicon = keepOrRejectAmplicon($cmdLineParameters, $ampliconID, $junctionType, $computedConfidenceIndex, $leftElement, $rightElement);

		# If the feature has passed all filters we keep it
		push(@filteredFeatures, $currentFeature) if ($keepAmplicon == 1);
	}

	return \@filteredFeatures;
}


sub determineConfidenceIndex {

	# Retrieval of parameters
	my ($junctionType, $leftExtremity, $rightExtremity) = @_;

	# Initializations
	my $confidenceIndex = 'medium';

	# Score a Confidence Index (high|medium|low)
	# It represents the probability for an ISBP to be locus-specific
	if ($junctionType =~ /^(\S+)__(\S+)$/) {
		$confidenceIndex = 'low' if ($1 eq $2);
	}

	if ($leftExtremity eq 'yes' or $rightExtremity eq 'yes') {
		$confidenceIndex = 'high';
	}

	return $confidenceIndex;
}


sub keepOrRejectAmplicon {

	# Retrieval of parameters
	my ($cmdLineParameters, $ampliconID, $junctionType, $confidenceIndex, $leftElement, $rightElement) = @_;

	# Initializations
	my $mark = 0;

	# Reject Unknown ISBPs if needed
	if ($cmdLineParameters->{'Reject_unknown'} =~ /yes|y|true/i) {
		if ($junctionType =~ /unknown/i) {
			$mainLogger->debug('Amplicon "' . $ampliconID . '" with Unknown like junction type (' . $junctionType . ') will not be written in the filtered output file !');
			return 0;
		}
	}

	# Reject ISBPs with 2 identical TEs if needed
	if ($cmdLineParameters->{'Reject_identical'} =~ /yes|y|true/i) {
		if ($junctionType =~ /^(\S+)__(\S+)$/) {
			if ($1 eq $2) {
				$mainLogger->debug('Amplicon "' . $ampliconID . '" has 2 identical TEs (' . $junctionType . ') and will not be written in the filtered output file !');
				return 0;
			}
		}
	}

	# Reject uncomplete ISBPs
	if ($cmdLineParameters->{'Reject_uncomplete'} =~ /yes|y|true/i) {
		if ($leftElement !~ /complete|consensus|MITE/i && $rightElement !~ /complete|consensus|MITE/i) {
			$mainLogger->debug('Amplicon "' . $ampliconID . '" has no valid keyword in its TE descriptions and will not be written in the filtered output file !');
			return 0;

		}
	}

	# Confidence Mark
	if (scalar(@{$cmdLineParameters->{'Confidence_filter_levels'}}) > 0) {
		foreach my $validConfidenceIndex (@{$cmdLineParameters->{'Confidence_filter_levels'}}) {
			if ($confidenceIndex =~ /$validConfidenceIndex/) {
				$mark = 1;
				$mainLogger->debug('Amplicon "' . $ampliconID . '" has passed the Confidence filter !');
				last;
			}
		}
	}

	# Junction type mark
	if (scalar(@{$cmdLineParameters->{'Junction_filter_list'}}) > 0) {
		foreach my $requestedJunctionType (@{$cmdLineParameters->{'Junction_filter_list'}}) {
			if ($junctionType eq $requestedJunctionType) {
				$mark = 1;
				$mainLogger->debug('Amplicon "' . $ampliconID . '" has passed the Junction type filter !');
				last;
			}
		}
	}

	return $mark;
}


############################################
## Output files writing related methods
############################################

# Moved to module Writers.pm


############################################
## Logging related methods
############################################

sub createLogger {

	# Retrieval of parameters
	my ($debugMode, $help, $printVersion, $nbArguments) = @_;

	# Create Logger and define basic level
	my $logger = get_logger('');
	$logger->level('INFO');

	# Screen Appender
	my $appenderColors = { 'TRACE' => 'blue', 'DEBUG' => 'yellow', 'INFO' => '', 'WARN' => 'bold magenta', 'ERROR' => 'red', 'FATAL' => 'underline bold red' };
	my $std_appender = Log::Log4perl::Appender->new("Log::Log4perl::Appender::ScreenColoredLevels", 'name' => 'screen_log', 'stderr' => 0, 'color' => $appenderColors);
	$logger->add_appender($std_appender);

	# Layout for Screen Appender
	my $std_layout = Log::Log4perl::Layout::PatternLayout->new("%m%n");
	$std_appender->layout($std_layout);

	# Create file appenders if needed only
	if (!$help && !$printVersion && $nbArguments > 0) {
		# Basic file Appender
		my $basic_file_appender = Log::Log4perl::Appender->new(	"Log::Log4perl::Appender::File", name => 'basic_log', mode => 'write', filename  => 'isbpFinder.log');
		$basic_file_appender->layout($std_layout);
		$basic_file_appender->threshold('INFO');
		$logger->add_appender($basic_file_appender);

		# Add appender for debug if needed
		if ($debugMode) {

			my $debug_appender = Log::Log4perl::Appender->new("Log::Log4perl::Appender::File", name => 'debug_log', mode => 'write', filename  => 'isbpFinder.debug');
			$logger->add_appender($debug_appender);

			my $debug_layout = Log::Log4perl::Layout::PatternLayout->new("%d %5p> %F{1}:%L %M - %m%n");
			$debug_appender->layout($debug_layout);

			$logger->level('DEBUG');
		}
	}

	return $logger;
}


############################################
## Configuration loading related methods
############################################

# Moved to module Configuration_loader.pm


############################################
## Parameters check related methods
############################################

sub checkParameters {

	# Retrieval of parameters
	my ($parameters, $configuration) = @_;

	# Check "Commons" parameters
	if (!defined($parameters->{'Output_format'})) {
		$mainLogger->warn('Warning: No "format|f" parameter specified ! Default value ("' . $configuration->{'Commons'}->{'outputFormat'} . '") will be used...');
		$parameters->{'Output_format'} = $configuration->{'Commons'}->{'outputFormat'};
	} elsif ($parameters->{'Output_format'} !~ /^(csv|embl|gff|all)$/i) {
		$mainLogger->warn('Warning: The value (' . $parameters->{'Output_format'} . ') of the "format|f" parameter given to this program is not valid ! Default value ("' . $configuration->{'Commons'}->{'outputFormat'} . '") will be used !');
		$parameters->{'Output_format'} = $configuration->{'Commons'}->{'outputFormat'};
	}

	if (!defined($parameters->{'Output_prefix'})) {
		$mainLogger->warn('Warning: No "output_prefix|o" parameter specified ! Output files names will be derived from input file names...');
	}

	if (!defined($parameters->{'Field_delimiter'})) {
		$mainLogger->warn('Warning: No "delimiter|del" parameter specified ! Default value ("' . $configuration->{'Commons'}->{'csvFieldDelimiter'} . '") will be used...');
		$parameters->{'Field_delimiter'} = $configuration->{'Commons'}->{'csvFieldDelimiter'};
	} elsif ($parameters->{'Field_delimiter'} !~ /^(;|tab)$/i) {
		$mainLogger->warn('Warning: The value (' . $parameters->{'Field_delimiter'} . ') of the "delimiter|del" parameter given to this program is not valid ! Default value ("' . $configuration->{'Commons'}->{'csvFieldDelimiter'} . '") will be used !');
		$parameters->{'Field_delimiter'} = $configuration->{'Commons'}->{'csvFieldDelimiter'};
	}

	if (defined($parameters->{'Field_delimiter'}) && $parameters->{'Field_delimiter'} eq 'tab') {
		$parameters->{'Field_delimiter'} = "\t";
	}

	if (!defined($parameters->{'Number_of_thread'})) {
		$mainLogger->warn('Warning: No "thread|cpu" parameter specified ! Default value ("' . $configuration->{'Commons'}->{'nbThread'} . '") will be used...');
		$parameters->{'Number_of_thread'} = $configuration->{'Commons'}->{'nbThread'};
	} elsif ($parameters->{'Number_of_thread'} !~ /^[0-9]+$/) {
		$mainLogger->warn('Warning: The value (' . $parameters->{'Number_of_thread'} . ') of the "thread|cpu" parameter given to this program is not valid ! Default value ("' . $configuration->{'Commons'}->{'nbThread'} . '") will be used !');
		$parameters->{'Number_of_thread'} = $configuration->{'Commons'}->{'nbThread'};
	}

	# Check "RepeatMasker" parameters
	if (defined($parameters->{'Execute_RepeatMasker'}) || defined($parameters->{'Existing_Repeatmasker_file'})) {
		if (!defined($parameters->{'RepeatMasker_library'})) {
			$mainLogger->warn('Warning: No "lib|l" parameter specified ! Default value ("' . $configuration->{'RepeatMasker'}->{'defaultLibrary'} . '") will be used...');
			$parameters->{'RepeatMasker_library'} = $configuration->{'RepeatMasker'}->{'defaultLibrary'};
		}
	}

	if (defined($parameters->{'RepeatMasker_library'})) {
		Misc::checkFileExistence($parameters->{'RepeatMasker_library'}, 'RepeatMasker library');
	}

	if (defined($parameters->{'Existing_Repeatmasker_file'})) {
		Misc::checkFileExistence($parameters->{'Existing_Repeatmasker_file'}, 'RepeatMasker result');
	}

	if (!defined($parameters->{'Use_quick_search'})) {
		$mainLogger->warn('Warning: No "quick|q" parameter specified ! Default value ("' . $configuration->{'RepeatMasker'}->{'useQuickSearch'} . '") will be used...');
		$parameters->{'Use_quick_search'} = $configuration->{'RepeatMasker'}->{'useQuickSearch'};
	} elsif ($parameters->{'Use_quick_search'} !~ /^(y|n|yes|no|true|false)$/i) {
		$mainLogger->warn('Warning: The value (' . $parameters->{'Use_quick_search'} . ') of the "quick|q" parameter given to this program is not valid ! Default value ("' . $configuration->{'RepeatMasker'}->{'useQuickSearch'} . '") will be used !');
		$parameters->{'Use_quick_search'} = $configuration->{'RepeatMasker'}->{'useQuickSearch'};
	}

	# Check "Primer3" parameters
	if (!defined($parameters->{'Minimum_amplicon_length'})) {
		$mainLogger->warn('Warning: No "min_amplicon_length|min" parameter specified ! Default value ("' . $configuration->{'Primer3'}->{'minPrimerProductSize'} . '") will be used...');
		$parameters->{'Minimum_amplicon_length'} = $configuration->{'Primer3'}->{'minPrimerProductSize'};
	} elsif ($parameters->{'Minimum_amplicon_length'} !~ /^[0-9]+$/) {
		$mainLogger->warn('Warning: The value (' . $parameters->{'Minimum_amplicon_length'} . ') of the "min_amplicon_length|min" parameter given to this program is not valid ! Default value ("' . $configuration->{'Primer3'}->{'minPrimerProductSize'} . '") will be used !');
		$parameters->{'Minimum_amplicon_length'} = $configuration->{'Primer3'}->{'minPrimerProductSize'};
	}

	if ($parameters->{'Minimum_amplicon_length'} < 60) {
		$mainLogger->warn('Warning: The minimum amplicon length is set to a value lower than 60 -> Primerq prediction might deliver incorrect results !');
	}

	if (!defined($parameters->{'Maximum_amplicon_length'})) {
		$mainLogger->warn('Warning: No "max_amplicon_length|max" parameter specified ! Default value ("' . $configuration->{'Primer3'}->{'maxPrimerProductSize'} . '") will be used...');
		$parameters->{'Maximum_amplicon_length'} = $configuration->{'Primer3'}->{'maxPrimerProductSize'};
	} elsif ($parameters->{'Maximum_amplicon_length'} !~ /^[0-9]+$/) {
		$mainLogger->warn('Warning: The value (' . $parameters->{'Maximum_amplicon_length'} . ') of the "max_amplicon_length|max" parameter given to this program is not valid ! Default value ("' . $configuration->{'Primer3'}->{'maxPrimerProductSize'} . '") will be used !');
		$parameters->{'Maximum_amplicon_length'} = $configuration->{'Primer3'}->{'maxPrimerProductSize'};
	}

	# Check Filters
	if (scalar(@{$parameters->{'Confidence_filter_levels'}}) == 0) {
		if (scalar(@{$configuration->{'Filters'}->{'confidenceFilterLevels'}}) > 0) {
			$mainLogger->warn('Warning: No "filter_by_confidence|fc" parameter specified ! Default value ("' . join(' ', @{$configuration->{'Filters'}->{'confidenceFilterLevels'}}) . '") will be used...');
			$parameters->{'Confidence_filter_levels'} = $configuration->{'Filters'}->{'confidenceFilterLevels'};
		} else {
			$mainLogger->warn('Warning: No "filter_by_confidence|fc" parameter specified and no default value defined in the XML configuration file => Filter disabled');
		}
	} else {
		foreach my $level (@{$parameters->{'Confidence_filter_levels'}}) {
			if ($level !~ /^(high|medium|low)$/i) {
				$mainLogger->warn('Warning: One of the value (' . $level . ') of the "filter_by_confidence|fc" parameter given to this program is not valid ! Execution canceled !');
				$mainLogger->warn('');
				displayHelp($configuration, 3);
			}
		}
	}

	if (!defined($parameters->{'Reject_uncomplete'})) {
		$mainLogger->warn('Warning: No "filter_by_description|fd" parameter specified ! Default value ("' . $configuration->{'Filters'}->{'descriptionFilterSwitch'} . '") will be used...');
		$parameters->{'Reject_uncomplete'} = $configuration->{'Filters'}->{'descriptionFilterSwitch'};
	} elsif ($parameters->{'Reject_uncomplete'} !~ /^(y|n|yes|no|true|false)$/i) {
		$mainLogger->warn('Warning: The value (' . $parameters->{'Reject_uncomplete'} . ') of the "filter_by_description|fd" parameter given to this program is not valid ! Default value ("' . $configuration->{'Filters'}->{'descriptionFilterSwitch'} . '") will be used !');
		$parameters->{'Reject_uncomplete'} = $configuration->{'Filters'}->{'descriptionFilterSwitch'};
	}

	if (scalar(@{$parameters->{'Junction_filter_list'}}) == 0) {
		if (scalar(@{$configuration->{'Filters'}->{'junctionFilterList'}}) > 0) {
			$mainLogger->warn('Warning: No "filter_by_jonction_type|fj" parameter specified ! Default value ("' . join(' ', @{$configuration->{'Filters'}->{'junctionFilterList'}}) . '") will be used...');
			$parameters->{'Junction_filter_list'} = $configuration->{'Filters'}->{'junctionFilterList'};
		} else {
			$mainLogger->warn('Warning: No "filter_by_jonction_type|fj" parameter specified and no default value defined in the XML configuration file => Filter disabled');
		}
	} else {
		foreach my $junction (@{$parameters->{'Junction_filter_list'}}) {
			if ($junction !~ /^\w+__\w+$/i) {
				$mainLogger->warn('Warning: One of the value (' . $junction . ') of the "filter_by_jonction_type|fj" parameter given to this program is not valid ! Execution canceled !');
				$mainLogger->warn('');
				displayHelp($configuration, 3);
			}
		}
	}

	if (!defined($parameters->{'Reject_identical'})) {
		$mainLogger->warn('Warning: No "reject_identical_te|fi" parameter specified ! Default value ("' . $configuration->{'Filters'}->{'identicalFilterSwitch'} . '") will be used...');
		$parameters->{'Reject_identical'} = $configuration->{'Filters'}->{'identicalFilterSwitch'};
	} elsif ($parameters->{'Reject_identical'} !~ /^(y|n|yes|no|true|false)$/i) {
		$mainLogger->warn('Warning: The value (' . $parameters->{'Reject_identical'} . ') of the "filter_identical_te|fi" parameter given to this program is not valid ! Default value ("' . $configuration->{'Filters'}->{'identicalFilterSwitch'} . '") will be used !');
		$parameters->{'Reject_identical'} = $configuration->{'Filters'}->{'identicalFilterSwitch'};
	}

	if (!defined($parameters->{'Reject_unknown'})) {
		$mainLogger->warn('Warning: No "filter_unknown_te|fu" parameter specified ! Default value ("' . $configuration->{'Filters'}->{'unknownFilterSwitch'} . '") will be used...');
		$parameters->{'Reject_unknown'} = $configuration->{'Filters'}->{'unknownFilterSwitch'};
	} elsif ($parameters->{'Reject_unknown'} !~ /^(y|n|yes|no|true|false)$/i) {
		$mainLogger->warn('Warning: The value (' . $parameters->{'Reject_unknown'} . ') of the "filter_unknown_te|fu" parameter given to this program is not valid ! Default value ("' . $configuration->{'Filters'}->{'unknownFilterSwitch'} . '") will be used !');
		$parameters->{'Reject_unknown'} = $configuration->{'Filters'}->{'unknownFilterSwitch'};
	}

	# Check for incompatibilities
	if ($parameters{'Execute_RepeatMasker'} && $parameters{'Existing_Repeatmasker_file'}) {
		$mainLogger->error('Error: Options "rm" and "dorm" are incompatible ! Please select one of them and relaunch your analysis.');
		displayHelp($configuration, 3);
	}

	$mainLogger->debug('Analysis parameters:');
	$mainLogger->debug(Dumper($parameters));

	return 0; # SUCCESS
}


##############################################
## Help and other screen display methods
##############################################

sub displayHelp {

	# Retrieval of parameters
	my ($configuration, $delay) = @_;

	# Initializations
	my $prog = basename($0);

	my $Readable_Description_filter_switch = ($configuration->{'Filters'}->{'descriptionFilterSwitch'} eq 'yes') ? 'be used' : 'not be used';
	my $Readable_Identical_filter_switch = ($configuration->{'Filters'}->{'identicalFilterSwitch'} eq 'yes') ? 'be used' : 'not be used';
	my $Readable_Unknown_filter_switch = ($configuration->{'Filters'}->{'unknownFilterSwitch'} eq 'yes') ? 'be used' : 'not be used';

	# Sleep time
	sleep($delay) if (defined($delay));

	# Help
	$mainLogger->info('##################################################');
	$mainLogger->info('###        ' . $prog . ' - HELP section        ###');
	$mainLogger->info('##################################################');
	$mainLogger->info('');

	$mainLogger->info('# VERSION:   ' . $version . ' (' . $lastModificationDate . ')');
	$mainLogger->info('# AUTHORS:   Frederic Choulet, Aurélien Bernard');
	$mainLogger->info('');

	$mainLogger->info('USAGE:');
	$mainLogger->info("\t" . $prog . ' [--dorm | --rm RepeatMasker_file] [--lib Repeat_library] [--output_prefix outfilePrefixName] [OPTIONS] fasta_file1 fasta_file2 ...');
	$mainLogger->info('');

	$mainLogger->info('EXAMPLES:');
	$mainLogger->info('');
	$mainLogger->info("\t" . $prog . ' --dorm --lib /usr/local/db/TREP.fas --output_prefix MyOutputPrefix --format all  MyFastaFile1.fas MyFastaFile2.fas');
	$mainLogger->info("\t" . $prog . ' --rm MyRmFile.out.xm --lib /usr/local/db/TREP.fas --output_prefix MyOutputPrefix --format all  MyFastaFile1.fas MyFastaFile2.fas');
	$mainLogger->info('');

	$mainLogger->info('OPTIONS:');

	$mainLogger->info('');
	$mainLogger->info("    " . 'Commons/Miscellaneous options:');
	$mainLogger->info('');

	$mainLogger->info("\t" . '--directory/-d <string>: Name of isbpFinder execution directory (optional).');
	$mainLogger->info("\t" . '                         If not specified, a default time-based name will be used');
	$mainLogger->info('');

	$mainLogger->info("\t" . '--thread/-cpu <integer>: Number of thread to use for multithread tools (RepeatMasker/Blast)');
	$mainLogger->info("\t" . '                         If not specified, the following default value will be used: ' . $configuration->{'Commons'}->{'nbThread'});
	$mainLogger->info('');

	$mainLogger->info("\t" . '--config/-c <path>: Path to a custom XML configuration file (optional)');
	$mainLogger->info('');

	$mainLogger->info("\t" . '--help/-h: Print this help message');
	$mainLogger->info('');

	$mainLogger->info("\t" . '--version/-v: Print version number and last modification date');
	$mainLogger->info('');

	$mainLogger->info("\t" . '--debug/-d: Activate full debug mode');
	$mainLogger->info('');

	$mainLogger->info('');
	$mainLogger->info("   " . 'Output related options:');
	$mainLogger->info('');

	$mainLogger->info("\t" . '--format/-f <string>: Output format (optional). Possible values are [csv|embl|gff|all]');
	$mainLogger->info("\t" . '                      If not specified, the following default value will be used: ' . $configuration->{'Commons'}->{'outputFormat'});
	$mainLogger->info('');

	$mainLogger->info("\t" . '--delimiter/-del <string>: Character to use to delimitate field in the csv output. Possible values are: [;|tab]');
	$mainLogger->info("\t" . '                           If not specified, the following default value will be used: ' . $configuration->{'Commons'}->{'csvFieldDelimiter'});
	$mainLogger->info('');

	$mainLogger->info("\t" . '--output_prefix/-o <file>: Output files prefix name');
	$mainLogger->info("\t" . '                           If not specified, a clean name derived from the input file and sequence names will be used');
	$mainLogger->info('');

	$mainLogger->info('');
	$mainLogger->info("    " . 'RepeatMasker related options:');
	$mainLogger->info('');

	$mainLogger->info("\t" . '--dorm: Run RepeatMasker for each FASTA sequence before the primer design');
	$mainLogger->info("\t" . '        If not used, RepeatMasler will not be executed');
	$mainLogger->info("\t" . '        * Incompatible with the --rm option *');
	$mainLogger->info('');

	$mainLogger->info("\t" . '--rm <path>: Path to a precomputed RepeatMasker result file (standard output, not XM format)');
	$mainLogger->info("\t" . '             * Incompatible with the --dorm option *');
	$mainLogger->info('');

	$mainLogger->info("\t" . '--lib/-l <path>: Path to the RepeatMasker library (in FASTA format) to use (--dorm) / used (--rm)');
	$mainLogger->info("\t" . '                 * Mandatory for both --dorm and --rm mode *');
	$mainLogger->info('');

	$mainLogger->info("\t" . '--quick/-q <string>: Run RepeatMasker with the -q option (-q is used to speed up). Possible values are [y|n|yes|no|true|false]');
	$mainLogger->info("\t" . '                     If not used, the -q option will not be used');
	$mainLogger->info('');

	$mainLogger->info('');
	$mainLogger->info("    " . 'Primer3 related options:');
	$mainLogger->info('');

	$mainLogger->info("\t" . '--min_amplicon_length/-min <integer>: Minimum primer product size / Amplicon length');
	$mainLogger->info("\t" . '                                      If not specified, the following default value will be used: ' . $configuration->{'Primer3'}->{'minPrimerProductSize'});
	$mainLogger->info('');

	$mainLogger->info("\t" . '--max_amplicon_length/-max <integer>: Maximum primer product size / Amplicon length');
	$mainLogger->info("\t" . '                                      If not specified, the following default value will be used: ' . $configuration->{'Primer3'}->{'maxPrimerProductSize'});
	$mainLogger->info('');

	$mainLogger->info('');
	$mainLogger->info("    " . 'Filtering options for CSV output:');
	$mainLogger->info('');
	$mainLogger->info("     " . 'Note: The following filters can be used to define a subset of ISBPs that will be written in an additional output file (the .filtered output file)');
	$mainLogger->info("     " . '      To be written in the filtered output file an ISBP must pass every activated filters');
	$mainLogger->info('');

	$mainLogger->info("\t" . '--filter_by_confidence/-fc <string>: Filter ISBPs based on the confidence level. Possible values are [high|medium|low]');
	$mainLogger->info("\t" . '                                     Enter a list of space separated confidence level (Ex: -fc high medium) or use this option several times (Ex: -fc high -fc medium)');
	$mainLogger->info("\t" . '                                     For example, "high medium" will allow ISBPs with a high or medium confidence level to be written in the filtered CSV output file');
	$mainLogger->info("\t" . '                                     If not specified, the following default value will be used: ' . join(' ', @{$configuration->{'Filters'}->{'confidenceFilterLevels'}}));
	$mainLogger->info('');

	$mainLogger->info("\t" . '--filter_by_jonction_type/-fj <array>: Filter ISBPs based on their junction type');
	$mainLogger->info("\t" . '                                       Enter a list of space separated junction type names (Ex: -fj Cacta__Cacta Gypsy__Gypsy)');
	$mainLogger->info("\t" . '                                       or use this option several times (Ex: -fj Cacta__Cacta -fj Gypsy__Gypsy)');
	$mainLogger->info("\t" . '                                       ISBPs with junction types listed with this parameter can be written in the filtered CSV output file');
	$mainLogger->info("\t" . '                                       If not specified, there will be no filtering based on junction names');
	$mainLogger->info('');

	$mainLogger->info("\t" . '--reject_uncomplete/-ruc <string>: Reject ISBPs based on the description of the TE predicted on each side of the junction. Possible values are [y|n|yes|no|true|false]');
	$mainLogger->info("\t" . '                                   Do not write ISBPs without one of the following keyword (on either side) in the filtered CSV output file: "complete|consensus|MITE"');
	$mainLogger->info("\t" . '                                   If not specified, this filter will ' . $Readable_Description_filter_switch);
	$mainLogger->info('');

	$mainLogger->info("\t" . '--reject_identical/-ri <string>: Reject ISBPs when two identical TEs are predicted on each side of the junction. Possible values are [y|n|yes|no|true|false]');
	$mainLogger->info("\t" . '                                 Do not write ISBPs like "Gypsy__Gypsy" or "CACTA__CACTA" (for example) in the filtered CSV output file');
	$mainLogger->info("\t" . '                                 If not specified, this filter will ' . $Readable_Identical_filter_switch);
	$mainLogger->info('');

	$mainLogger->info("\t" . '--reject_unknown/-ru <string>: Reject ISBPs when one or both TE comes from an "Unknown" superfamily. Possible values are [y|n|yes|no|true|false]');
	$mainLogger->info("\t" . '                               Do not write "Unknown__XXX", "XXX_Unknown" or "Unknown__Unknown" ISBPs in the filtered CSV output file');
	$mainLogger->info("\t" . '                               If not specified, this filter will ' . $Readable_Unknown_filter_switch);
	$mainLogger->info('');

	exit(0);
}


sub displayVersion {

	# Retrieval of parameters
	my ($programName, $version, $lastModificationDate) = @_;

	$mainLogger->info('###################################');
	$mainLogger->info('###        ' . basename($programName) . '        ###');
	$mainLogger->info('###################################');
	$mainLogger->info('');
	$mainLogger->info('Version: ' . $version);
	$mainLogger->info('');
	$mainLogger->info('Last modification date: ' . $lastModificationDate);
	$mainLogger->info('');

	exit(0);
}

############################################
## Miscellaneous functions
############################################

# Moved to module Misc.pm
