#!/usr/bin/env perl

package Configuration_loader;

# Basic Perl modules
use strict;
use warnings;
use diagnostics;

# Perl modules
use Log::Log4perl qw(get_logger);
use XML::Twig;
use File::Basename;

# Perl debug modules
use Data::Dumper;

# Custom modules
use Misc;

# Get log4Perl logger
my $mainLogger = get_logger('');


sub getConfiguration {

	# Retrieval of parameters
	my ($Ref_to_parameters, $programVersion) = @_;

	# Initializations
	my $defaultConfigFileName = 'default.xml';
	my ($configFileFullPath, $configFileType) = ('', '');

	# Get the full path of the XML configuration file
	if (defined($Ref_to_parameters->{'Config_file'})) {
		$configFileFullPath = $Ref_to_parameters->{'Config_file'};
		$configFileType = 'custom';

	} else {
		$configFileType = 'default';

		# Get default configuration file fullpath
		if (defined($ENV{"ISBPFINDER_ROOT"}) && -d Cwd::realpath($ENV{"ISBPFINDER_ROOT"}) . '/conf') {
			$configFileFullPath = Cwd::realpath($ENV{"ISBPFINDER_ROOT"}) . '/conf/' . $defaultConfigFileName;
			$mainLogger->debug('Default configuration file (ENV): ' . $configFileFullPath);
		} elsif (-d $FindBin::RealBin . '/../conf/') {
			$configFileFullPath = dirname($FindBin::RealBin) . '/conf/' . $defaultConfigFileName;
			$mainLogger->debug('Default configuration file (FindBin): ' . $configFileFullPath);
		} else {
			my $realExecutablePath = (-l $0) ? readlink(rel2abs($0)) : rel2abs($0);
			$configFileFullPath = dirname(dirname($realExecutablePath)) . '/conf/' . $defaultConfigFileName;
			$mainLogger->debug('Default configuration file ($0 style): ' . $configFileFullPath);
		}
	}

	# Check file
	Misc::checkFileExistence($configFileFullPath, 'XML configuration');

	# Load custom or default XML configuration file
	$mainLogger->debug('Loading ' . $configFileType . ' XML configuration file: ' . basename($configFileFullPath));
	my $xmlConfiguration = loadConfigurationFile($configFileFullPath, $programVersion);

	$mainLogger->debug('Collected configuration data:');
	$mainLogger->debug(Dumper($xmlConfiguration));

	return $xmlConfiguration;
}


sub loadConfigurationFile {

	# Retrieval of parameters
	my ($configFileFullPath, $programVersion) = @_;

	# Initializations
	my $collectedConfig = {};

	# Initialize XML-Twig object
	my $twigObject = XML::Twig->new();
	$twigObject->parsefile($configFileFullPath);

	# Check configuration file version
	if ($twigObject->root->{'att'}->{'version'} ne $programVersion) {
		$mainLogger->logdie('Critical Error: The selected XML configuration file version (' . $twigObject->root->{'att'}->{'version'} . ') does not match isbpFinder version (' . $programVersion . ')');
	}

	my @sections= $twigObject->root->children('section');
	foreach my $section (@sections) {
		readSection($section, $collectedConfig)
	}
	$twigObject->purge();

	# Treat special configuration values (auto-generated associative arrays)
	$collectedConfig->{'Filters'}->{'confidenceFilterLevels'} = convertToArrayRef($collectedConfig->{'Filters'}->{'confidenceFilterLevels'});
	$collectedConfig->{'Filters'}->{'junctionFilterList'} = convertToArrayRef($collectedConfig->{'Filters'}->{'junctionFilterList'});

	$mainLogger->debug('All data have been successfully loaded !');

	return $collectedConfig;
}

sub readSection {

	# Retrieval of parameters
	my ($sectionXml, $hashDestination) = @_;

	# Initializations
	my $entriesCpt = 0;
	my $sectionName = trim($sectionXml->{'att'}->{'name'});
	my $clear = ($sectionXml->att_exists('clear')) ? 1 : 0;

	$mainLogger->logdie('Invalid section found. All sections must have a name attribute:' . "\n" . $sectionXml->start_tag()) if (!defined($sectionName) || trim($sectionName) eq '');
	$mainLogger->info('Overriding section [' . $sectionName . ']') if (defined($hashDestination->{$sectionName}));

	if (!defined($hashDestination->{$sectionName}) || (defined($hashDestination->{$sectionName}) && $clear)) {
		$hashDestination->{$sectionName} = {};
	}

	$entriesCpt = scalar(keys(%{$hashDestination->{$sectionName}}));
	foreach my $entry ($sectionXml->children('entry')) {
		readEntry($entry, $hashDestination->{$sectionName}, $sectionName, $entriesCpt++);
	}
}


sub readEntry {

	# Retrieval of parameters
	my ($entryXml, $hashDestination, $sectionName, $entryNumber) = @_;

	# Initializations
	my $key = $entryNumber;

	if (defined($entryXml->{'att'}->{'key'})) {
		$key = trim($entryXml->{'att'}->{'key'});
		$mainLogger->logdie('Invalid empty entry key found in section [' . $sectionName . ']') if ($key eq '');
	}

	my $value = trim($entryXml->text_only());
	$mainLogger->logdie('Invalid entry [' . $key . '] found in section [' . $sectionName . ']') if (($value ne '' and $entryXml->has_child('entry')));
	$mainLogger->info('Overriding value of entry [' . $key . '] in section [' . $sectionName . ']') if (defined($hashDestination->{$key}));

	if (!$entryXml->has_child('entry')) { # scalar value
		$hashDestination->{$key} = $value;
	} else {
		my $subEntriesCpt = 0;
		my $clear = ($entryXml->att_exists('clear')) ? 1 : 0;

		if (!defined($hashDestination->{$key}) || (defined($hashDestination->{$key}) && $clear)) {
			$hashDestination->{$key} = {};
		}

		$subEntriesCpt = scalar(keys(%{$hashDestination->{$key}}));
		foreach my $entry ($entryXml->children('entry')) {
			readEntry($entry, $hashDestination->{$key}, $sectionName, $subEntriesCpt++);
		}
	}
}


sub trim($) {
	# Retrieval of parameters
	my $string = shift;

	# Remove useless spaces
	if (defined($string)) {
		$string =~ s/^\s+//;
		$string =~ s/\s+$//;
	}

	return $string;
}


sub convertToArrayRef {

	# Retrieval of parameters
	my ($configurationEntry) = shift;

	# Initializations
	my @arrayForm = ();

	# Convert
	if (defined($configurationEntry) && ref($configurationEntry) eq 'HASH') {
		while (my ($key, $value) = each %{$configurationEntry}) {
			$arrayForm[$key] = $value;
		}
	}

	return \@arrayForm;
}

sub humanReadableDateInConfLoader {

	# Retrieval of parameters
	my $time = shift || time;

	# Split time string
	my ($seconde, $minute, $heure, $jour, $mois, $annee, $jour_semaine, $jour_annee, $heure_hiver_ou_ete) = localtime($time);
	$mois  += 1;
	$annee += 1900;

	# On rajoute 0 si le chiffre est compris entre 1 et 9
	foreach ( $seconde, $minute, $heure, $jour, $mois, $annee ) { s/^(\d)$/0$1/; }

	return $jour . '-' . $mois . '-' . $annee . ' ' . $heure . ':' . $minute . ':' . $seconde;
}

1;
