#!/usr/bin/env perl

package Feature_builders;

# Basic Perl modules
use strict;
use warnings;
use diagnostics;

# Perl modules
use Log::Log4perl qw(get_logger);

# Perl debug modules
use Data::Dumper;

# BioPerl modules
use Bio::SeqFeature::Generic;

# Get log4Perl logger
my $mainLogger = get_logger('');


sub buildRepeatRegionFeature {

	# Retrieval of parameters
	my ($repeatData, $featureNumber) = @_;

	# Initialization
	my $repeatRegionTag = {};

	# Generate attributes of the new feature
	$repeatRegionTag->{'Name'} = 'RepeatMasker_' . $repeatData->{'queryStart'} . '_' . $repeatData->{'queryEnd'} . '_Repeat_region_' . $featureNumber;
	$repeatRegionTag->{'ID'} = $repeatData->{'queryName'} . '_' . $repeatRegionTag->{'Name'};
	$repeatRegionTag->{'Target'} = [$repeatData->{'repeatName'}, $repeatData->{'hitStart'}, $repeatData->{'hitEnd'}]; # Target Tag must be a reference to a table (anonymous or not)

	my $repeatRegionFeature = Bio::SeqFeature::Generic->new(
							 -seq_id      => $repeatData->{'queryName'},
							 -source_tag  => 'RepeatMasker',
							 -primary_tag => 'repeat_region',
							 -start       => $repeatData->{'queryStart'},
							 -end         => $repeatData->{'queryEnd'},
							 -strand      => $repeatData->{'strand'},
							 -tag         => $repeatRegionTag
							);

	# Add custom tags
	$repeatRegionFeature->add_tag_value('Note', 'Repeat_description: ' . $repeatData->{'originalDescription'});
	$repeatRegionFeature->add_tag_value('Note', 'Repeat_length: ' . ($repeatData->{'hitEnd'} + $repeatData->{'hitUnmasked'}));
	$repeatRegionFeature->add_tag_value('Note', 'Smith-waterman score: ' . $repeatData->{'score'});
	$repeatRegionFeature->add_tag_value('Note', 'Query_Unmasked: ' . $repeatData->{'queryUnmasked'});
	$repeatRegionFeature->add_tag_value('Note', 'Hit_Unmasked: ' . $repeatData->{'hitUnmasked'});
	$repeatRegionFeature->add_tag_value('Note', 'Start_at_extremity: ' . $repeatData->{'startAtExtremity'});
	$repeatRegionFeature->add_tag_value('Note', 'End_at_extremity: ' . $repeatData->{'endAtExtremity'});
	$repeatRegionFeature->add_tag_value('color', 5) if ($repeatData->{'startAtExtremity'} eq 'yes' or $repeatData->{'endAtExtremity'} eq 'yes');
	$repeatRegionFeature->add_tag_value('label', $repeatData->{'shortDescription'});
	$repeatRegionFeature->add_tag_value('Ontology_term', getOntology('repeat_region'));

	# Set the Human readable name
	$repeatRegionFeature->display_name($repeatRegionTag->{'Name'});

	$mainLogger->debug('RepeatMasker feature:');
	$mainLogger->debug(Dumper($repeatRegionFeature));

	return $repeatRegionFeature;
}


sub buildAmpliconFeature {

	# Retrieval of parameters
	my ($primerData, $sequenceID, $subSequenceStart, $targetNumber) = @_;

	# Initializations
	$primerData->{'Start'} = ($primerData->{'Left_position'} + $subSequenceStart);
	$primerData->{'End'} = ($primerData->{'Right_position'} + $subSequenceStart);

	# Generate attributes of the new feature
	my $NewFeatureAttributes = {};
	$NewFeatureAttributes->{'Name'} = 'isbpFinder_' . $primerData->{'Start'} . '_' . $primerData->{'End'} . '_Amplicon_' . sprintf("%03d", $primerData->{'Primer_pair_number'});
	$NewFeatureAttributes->{'ID'} = $sequenceID . '_' . $NewFeatureAttributes->{'Name'};

	# Build feature
	my $ampliconFeature = Bio::SeqFeature::Generic->new(
		'-seq_id'  => $sequenceID,
		'-source_tag' => 'isbpFinder',
		'-primary_tag' => 'Amplicon',
		'-start' => $primerData->{'Start'},
		'-end' => $primerData->{'End'},
		'-strand' => 1,
		'-tag' => $NewFeatureAttributes
	);

	# Add tags
	$ampliconFeature->add_tag_value('color', 3);
	$ampliconFeature->add_tag_value('Note', 'Target_number: ' . $targetNumber);

	$ampliconFeature->add_tag_value('Note', 'Amplicon_size: ' . $primerData->{'Amplicon_size'});
	$ampliconFeature->add_tag_value('Note', 'Amplicon_GC_percent: ' . $primerData->{'Amplicon_GC_percent'});

	$ampliconFeature->add_tag_value('Note', 'Forward_primer: ' . $primerData->{'Left_sequence'});
	$ampliconFeature->add_tag_value('Note', 'Forward_primer_size: ' . $primerData->{'Left_size'});
	$ampliconFeature->add_tag_value('Note', 'Forward_TM: ' . $primerData->{'Left_TM'});
	$ampliconFeature->add_tag_value('Note', 'Forward_stability: ' . $primerData->{'Left_stability'});

	$ampliconFeature->add_tag_value('Note', 'Reverse_primer: ' . $primerData->{'Right_sequence'});
	$ampliconFeature->add_tag_value('Note', 'Reverse_primer_size: ' . $primerData->{'Right_size'});
	$ampliconFeature->add_tag_value('Note', 'Reverse_TM: ' . $primerData->{'Right_TM'});
	$ampliconFeature->add_tag_value('Note', 'Reverse_stability: ' . $primerData->{'Right_stability'});

	$ampliconFeature->add_tag_value('Ontology_term', getOntology('Amplicon'));

	$ampliconFeature->add_tag_value('Amplicon_sequence', $primerData->{'Amplicon_sequence'}); # Will not be written in EMBL or GFF output files

	$ampliconFeature->display_name($NewFeatureAttributes->{'Name'});

	$mainLogger->debug('Amplicon feature:');
	$mainLogger->debug(Dumper($ampliconFeature));

	return $ampliconFeature;
}


sub updateAmpliconFeature {

	# Retrieval of parameters
	my ($currentAmpliconFeature, $junctionData, $teConfiguration, $rmLibraryIndex) = @_;

	# Initializations
	my ($leftOriginalDescription, $rightOriginalDescription) = ('')x2;
	my ($leftShortDescription, $rightShortDescription) = ('LowCopyDNA')x2;

	# Update the current amplicon feature
	if ($junctionData->{'Left_element'} ne 'no_element') {
		($leftOriginalDescription, $leftShortDescription) = RepeatMasker::getDescriptions($rmLibraryIndex, $junctionData->{'Left_element'}, $teConfiguration);
		$currentAmpliconFeature->add_tag_value('Left_element', $junctionData->{'Left_element'} . ' - ' . $leftOriginalDescription);
		$currentAmpliconFeature->add_tag_value('Left_extremity', $junctionData->{'Left_extremity'});

		if ($junctionData->{'Left_extremity'} eq 'yes' || ($junctionData->{'Left_extremity'} eq 'no' && $junctionData->{'Right_extremity'} eq 'no') ) {
			$currentAmpliconFeature->add_tag_value('Junction_position_on_query_sequence', $junctionData->{'Left_position_on_query'});
			$currentAmpliconFeature->add_tag_value('Junction_position_on_PCR_product', $junctionData->{'Left_position_on_PCR_product'});
		}
	}

	if ($junctionData->{'Right_element'} ne 'no_element') {
		($rightOriginalDescription, $rightShortDescription) = RepeatMasker::getDescriptions($rmLibraryIndex, $junctionData->{'Right_element'}, $teConfiguration);
		$currentAmpliconFeature->add_tag_value('Right_element', $junctionData->{'Right_element'} . ' - ' . $rightOriginalDescription);
		$currentAmpliconFeature->add_tag_value('Right_extremity', $junctionData->{'Right_extremity'});

		if ($junctionData->{'Right_extremity'} eq 'yes' || ($junctionData->{'Left_extremity'} eq 'no' && $junctionData->{'Right_extremity'} eq 'no') ) {
			$currentAmpliconFeature->add_tag_value('Junction_position_on_query_sequence', $junctionData->{'Right_position_on_query'});
			$currentAmpliconFeature->add_tag_value('Junction_position_on_PCR_product', $junctionData->{'Right_position_on_PCR_product'});
		}
	}

	$currentAmpliconFeature->add_tag_value('Junction_type', $leftShortDescription . '__' . $rightShortDescription);

	# Log
	$mainLogger->debug('Updated amplicon feature:');
	$mainLogger->debug(Dumper($currentAmpliconFeature));

	$mainLogger->info("\t" . '  --> Left/Right element are: ' . $junctionData->{'Left_element'} . ' <=> ' . $junctionData->{'Right_element'});
	$mainLogger->info("\t" . '  --> Junction type is: ' . join(',', $currentAmpliconFeature->get_tag_values('Junction_type')));

	if ($currentAmpliconFeature->has_tag('Junction_position_on_query_sequence')) {
		$mainLogger->info("\t" . '  --> Junction positions on the query sequence are: ' . join(', ', $currentAmpliconFeature->get_tag_values('Junction_position_on_query_sequence')));
	}
	if ($currentAmpliconFeature->has_tag('Junction_position_on_PCR_product')) {
		$mainLogger->info("\t" . '  --> Junction positions on the PCR product are: ' . join(', ', $currentAmpliconFeature->get_tag_values('Junction_position_on_PCR_product')));
	}

	return 0; # SUCCESS
}


sub getOntology {

	# Retrieval of parameters
	my ($type, $patternSize) = @_;

	if ($type =~ /repeat_region/i) {
		return 'SO:0000657'; # A region of sequence containing one or more repeat units
	} elsif ($type =~ /Amplicon/i) {
		return 'SO:0000006'; # Amplicon/ PCR_product = A region amplified by a PCR reaction
	} else {
		$mainLogger->warn('Warning: getOntology() cannot return the ontology of an unsupported feature type: ' . $type);
		return '';
	}
}

1;
