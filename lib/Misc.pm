#!/usr/bin/env perl

package Misc;

# Basic Perl modules
use strict;
use warnings;
use diagnostics;

# Perl modules
use File::Basename;

# Perl modules
use Log::Log4perl qw(get_logger);

# Get log4Perl logger
my $mainLogger = get_logger('');


sub checkFileExistence {

	# Retrieval of parameters
	my ($fileToCheck, $fileType) = @_;

	# Check
	if (! -e $fileToCheck || -z $fileToCheck) {
		$mainLogger->logdie('Critical Error: The following ' . $fileType . ' file does not exist or is empty: ' . basename($fileToCheck));
	}
}


sub humanReadableDate {

	# Retrieval of parameters
	my $time = shift || time;

	# Split time string
	my ($seconde, $minute, $heure, $jour, $mois, $annee, $jour_semaine, $jour_annee, $heure_hiver_ou_ete) = localtime($time);
	$mois  += 1;
	$annee += 1900;

	# On rajoute 0 si le chiffre est compris entre 1 et 9
	foreach ( $seconde, $minute, $heure, $jour, $mois, $annee ) { s/^(\d)$/0$1/; }

	return $jour . '-' . $mois . '-' . $annee . ' ' . $heure . ':' . $minute . ':' . $seconde;
}


sub jumpIn {

	# Retrieval of parameters
	my $directoryName = shift;

	# If the directory already exist we rename it
	if (-d $directoryName) {
		rename($directoryName, $directoryName . '.old.' . time()) or die('Could not rename directory: ' . $directoryName);
	}

	# Create directory
	mkdir($directoryName) or die('Could not create directory: ' . $directoryName);

	# Jump in
	chdir($directoryName) or die('Could not move to directory: ' . $directoryName);

	return 0; # SUCCESS
}


# Build an hash table from the complex Note field of a feature
# Example: if you have a feature with the following Note field..
# $ampliconFeature->add_tag_value('Note', 'Target_number: ' . 10);
# Then you will get a hash table with one entry: 'Target_number' (value = 10)
sub getHashFromNote {

	# Retrieval of parameters
	my $feature = shift;

	# Initializations
	my %noteData = ();

	foreach my $noteCouple ($feature->get_tag_values('Note')) {
		my ($key, $value) = split(': ', $noteCouple);
		$noteData{$key} = $value;
	}

	return \%noteData;
}


sub determineGCPercent {

	# Retrieval of parameters
	my $seq = shift;

	# Initializations
	my ($gcPercent, $gcCounter, $erCounter) = (0.00, 0, 0);
	my $sequenceLength = length($seq);

	# Determine the GC percent of the sequence
	foreach (0 .. $sequenceLength - 1) {
		my $base = substr($seq, $_, 1);
		$base=~ /[acgt]/i or $erCounter += 1;
		$base=~ /[cg]/i and $gcCounter += 1;
	}

	$gcPercent = sprintf("%.2f", (($gcCounter) / ($sequenceLength - $erCounter) * 100)) if ($sequenceLength != $erCounter);

	return $gcPercent;
}

1;
