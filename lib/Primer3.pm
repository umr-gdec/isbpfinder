#!/usr/bin/env perl

package Primer3;

# Basic Perl modules
use strict;
use warnings;
use diagnostics;

# Perl modules
use Log::Log4perl qw(get_logger);

# Perl debug modules
use Data::Dumper;

# BioPerl modules
use Bio::Tools::Primer3;

# Get log4Perl logger
my $mainLogger = get_logger('');


sub buildTargets {

	# Retrieval of parameters
	my ($currentRepeatFeature, $unmasked) = @_;

	# Initializations
	my @targets = ();
	my $limit = 30;

	$mainLogger->info("\t" . '-> Building targets..');

	# Initialize targets
	$targets[0] = {'number' => 1, 'repeatFlankingRegionSize' => 10, 'position' => 'none'};
	$targets[1] = {'number' => 2, 'repeatFlankingRegionSize' => 10, 'position' => 'none'};

	# Define target's start/stop
	if ($currentRepeatFeature->start() <= $limit and $unmasked < -$limit) { $targets[1]->{'position'} = $currentRepeatFeature->end() - $targets[1]->{'repeatFlankingRegionSize'}; }
	elsif ($currentRepeatFeature->start() > $limit and $unmasked >= -$limit) { $targets[0]->{'position'} = $currentRepeatFeature->start() - $targets[1]->{'repeatFlankingRegionSize'}; }
	elsif ($currentRepeatFeature->start() > $limit and $unmasked < -$limit) { $targets[0]->{'position'} = $currentRepeatFeature->start() - $targets[0]->{'repeatFlankingRegionSize'}; $targets[1]->{'position'} = $currentRepeatFeature->end() - $targets[1]->{'repeatFlankingRegionSize'}; }

	$mainLogger->debug('Generated targets:');
	$mainLogger->debug(Dumper(@targets));

	return \@targets;
}


sub preparePrimer3SubSequence {

	# Retrieval of parameters
	my ($sequenceObject, $currentTarget, $currentRepeatFeature, $closeRepeatsData, $maxAmpliconLength) = @_;

	# Initializations
	my %subSequenceData = ();

	# Determine subsequence Start/Stop
	$subSequenceData{'start'} = $currentTarget->{'position'} - $maxAmpliconLength - 100;
	$subSequenceData{'stop'}  = $currentTarget->{'position'} + $maxAmpliconLength + 100 + (2 * $currentTarget->{'repeatFlankingRegionSize'});

	# Avoid subsequence Start/Stop outside of the sequence
	$subSequenceData{'start'} < 1 and $subSequenceData{'start'} = 1;
	$subSequenceData{'stop'}  > $sequenceObject->length() and $subSequenceData{'stop'} = $sequenceObject->length();

	# Controls
	if ($currentTarget->{'number'} == 1) {
		# The subsequence cannot end after the end of the current repeat
		$subSequenceData{'stop'} > $currentRepeatFeature->end() and $subSequenceData{'stop'} = $currentRepeatFeature->end();

		# The start of the subsequence can't be located before the start of the previous repeat (unless the repeats are really close (50 bases))
		if (defined($closeRepeatsData->{'previousStart'})) {
			if ($closeRepeatsData->{'previousStart'} > $subSequenceData{'start'} and $closeRepeatsData->{'previousStart'} < $currentRepeatFeature->start() - 50) {
				$subSequenceData{'start'} = $closeRepeatsData->{'previousStart'};
			}
		}
	} else {
		# The subsequence cannot start before the start of the current repeat
		$subSequenceData{'start'} < $currentRepeatFeature->start() and $subSequenceData{'start'} = $currentRepeatFeature->start();

		# The end of the subsequence can't be located after the end of the next repeat (unless the repeats are really close (50 bases))
		if (defined($closeRepeatsData->{'nextEnd'})) {
			if ($closeRepeatsData->{'nextEnd'} < $subSequenceData{'stop'} and $closeRepeatsData->{'nextEnd'} > $currentRepeatFeature->end() + 50) {
				$subSequenceData{'stop'} = $closeRepeatsData->{'nextEnd'};
			}
		}
	}

	# Generate subsequence string and ID
	$subSequenceData{'sequence'} = $sequenceObject->subseq($subSequenceData{'start'}, $subSequenceData{'stop'});
	$subSequenceData{'ID'} = $sequenceObject->display_id() . '_subseq_' . $subSequenceData{'start'} . '_' . $subSequenceData{'stop'};

	$mainLogger->debug('Generated subsequence:');
	$mainLogger->debug(Dumper(\%subSequenceData));

	return \%subSequenceData;
}


sub generatePrimer3InputFile {

	# Retrieval of parameters
	my ($primer3InputFile, $subSequenceData, $currentTarget, $minAmpliconLength, $maxAmpliconLength, $numberOfPrediction) = @_;

	# Determine target start and length (SEQUENCE_TARGET)
	my $sequenceTargetLength = 2 * $currentTarget->{'repeatFlankingRegionSize'};
	my $sequenceTargetStart = ($currentTarget->{'position'} - $subSequenceData->{'start'} + 1);

	# Write Primer3 input file
	open (PRIM_IN, '>' . $primer3InputFile) or $mainLogger->logdie('Error: Cannot create/open file: ' . $primer3InputFile);

	print PRIM_IN 'SEQUENCE_ID=' . $subSequenceData->{'ID'} . "\n";
	print PRIM_IN 'SEQUENCE_TEMPLATE=' . $subSequenceData->{'sequence'} . "\n";
	#print PRIM_IN 'SEQUENCE=' . $subSequenceData->{'sequence'} . "\n";
	print PRIM_IN 'SEQUENCE_TARGET=' . $sequenceTargetStart . ',' . $sequenceTargetLength . "\n";
	print PRIM_IN 'PRIMER_THERMODYNAMIC_OLIGO_ALIGNMENT=' . 0 . "\n";
	print PRIM_IN 'PRIMER_PRODUCT_SIZE_RANGE=' . $minAmpliconLength . '-' . $maxAmpliconLength . "\n";
	print PRIM_IN 'PRIMER_NUM_RETURN=' . $numberOfPrediction . "\n";
	print PRIM_IN '=' . "\n";

	close (PRIM_IN);

	return 0; # SUCCESS
}


sub parsePrimer3Output_obj {

	# Retrieval of parameters
	my $primer3ResultFile = shift;

	# Initializations
	my $counter = 0;
	my @predictedPrimerPairs = ();

	# Use BioPerl to parse the Primer3 output file
	my $primer3Parser = Bio::Tools::Primer3->new( -file => $primer3ResultFile );

	# Get the number of predicted primer pair
	my $nbPrimerPair = $primer3Parser->number_of_results();

	if ($nbPrimerPair > 0) {
		while ( my $primedSeq = $primer3Parser->next_primer() ) {
			$counter++;

			my $leftPrimer = $primedSeq->get_primer('left');
			my ($leftPosition, $leftSize) = split(',', join(';', $leftPrimer->get_tag_values('PRIMER_LEFT')));

			my $rightPrimer = $primedSeq->get_primer('right');
			my ($rightPosition, $rightSize) = split(',', join(';', $rightPrimer->get_tag_values('PRIMER_RIGHT')));

			my %primerPair = (  'Primer_pair_number' => $counter,
								'Amplicon_size' => join(',', $primedSeq->get_tag_values('PRIMER_PAIR_PRODUCT_SIZE')),
								'Amplicon_sequence' => $primedSeq->amplicon()->seq(),
								'Amplicon_GC_percent' => Misc::determineGCPercent($primedSeq->amplicon()->seq()),

								'Left_sequence' => $leftPrimer->seq->seq(),
								'Left_position' => $leftPosition,
								'Left_size' => $leftSize,
								'Left_TM' => join(',', $leftPrimer->get_tag_values('PRIMER_LEFT_TM')),
								'Left_stability' => join(',', $leftPrimer->get_tag_values('PRIMER_LEFT_END_STABILITY')),

								'Right_sequence' => $rightPrimer->seq->seq(),
								'Right_position' => $rightPosition,
								'Right_size' => $rightSize,
								'Right_TM' => join(',', $rightPrimer->get_tag_values('PRIMER_RIGHT_TM')),
								'Right_stability' => join(',', $rightPrimer->get_tag_values('PRIMER_RIGHT_END_STABILITY'))
							);

			push(@predictedPrimerPairs, \%primerPair);
		}
	}

	$mainLogger->debug('Predicted Primer Pairs:');
	$mainLogger->debug(Dumper(@predictedPrimerPairs));
	$mainLogger->info("\t" . '  --> ' . scalar(@predictedPrimerPairs) . ' primer pair(s) found !');

	return \@predictedPrimerPairs;
}


sub parsePrimer3Output {

	# Retrieval of parameters
	my ($primer3ResultFile, $inputSequence) = @_;

	# Initializations
	my $counter = 0;
	my @predictedPrimerPairs = ();

	# Use BioPerl to parse the Primer3 output file
	my $primer3Parser = Bio::Tools::Primer3->new( -file => $primer3ResultFile );

	# Get the number of predicted primer pair
	my $nbPrimerPair = $primer3Parser->number_of_results();

	for (my $i = 0; $i < $nbPrimerPair; $i++) {
		my $primerPairData = $primer3Parser->primer_results($i);

		my ($leftPosition, $leftSize) = split(',', $primerPairData->{'PRIMER_LEFT'});
		my ($rightPosition, $rightSize) = split(',', $primerPairData->{'PRIMER_RIGHT'});

		my $ampliconSequence = substr($inputSequence, $leftPosition, $rightPosition - $leftPosition + 1);

		my %primerPair = (  'Primer_pair_number' => $i+1,
							'Amplicon_size' => $primerPairData->{'PRIMER_PAIR_PRODUCT_SIZE'},
							'Amplicon_sequence' => $ampliconSequence,
							'Amplicon_GC_percent' => Misc::determineGCPercent($ampliconSequence),

							'Left_sequence' => $primerPairData->{'PRIMER_LEFT_SEQUENCE'},
							'Left_position' => $leftPosition,
							'Left_size' => $leftSize,
							'Left_TM' => $primerPairData->{'PRIMER_LEFT_TM'},
							'Left_stability' => $primerPairData->{'PRIMER_LEFT_END_STABILITY'},

							'Right_sequence' => $primerPairData->{'PRIMER_RIGHT_SEQUENCE'},
							'Right_position' => $rightPosition,
							'Right_size' => $rightSize,
							'Right_TM' => $primerPairData->{'PRIMER_RIGHT_TM'},
							'Right_stability' => $primerPairData->{'PRIMER_RIGHT_END_STABILITY'}
						);

		push(@predictedPrimerPairs, \%primerPair);
	}

	$mainLogger->debug('Predicted Primer Pairs:');
	$mainLogger->debug(Dumper(@predictedPrimerPairs));
	$mainLogger->info("\t" . '  --> ' . scalar(@predictedPrimerPairs) . ' primer pair(s) found !');

	return \@predictedPrimerPairs;
}

1;
