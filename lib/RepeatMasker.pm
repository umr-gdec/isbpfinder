#!/usr/bin/env perl

package RepeatMasker;

# Basic Perl modules
use strict;
use warnings;
use diagnostics;

# Perl modules
use File::Copy;
use Log::Log4perl qw(get_logger);
use File::Basename;

# Perl debug modules
use Data::Dumper;

# Get log4Perl logger
my $mainLogger = get_logger('');


sub indexRepeatMaskerLibrary {

	# Retrieval of parameters
	my $libraryToIndex = shift;

	# Initializations
	my %libraryEntries = ();

	# Browse the fasta library
	my $fastaLibraryStream = Bio::SeqIO->new( '-file' => $libraryToIndex, '-format' => 'FASTA' );

	while ( my $libraryEntry = $fastaLibraryStream->next_seq() ){
		# Collect and clean the description
		my $cleanDescription = $libraryEntry->description();
		$cleanDescription =~ s/[\;\t]/ /g;

		# Save the ID=Description couple
		$libraryEntries{$libraryEntry->display_id()} = $cleanDescription;
	}

	return \%libraryEntries;
}


sub executeRepeatMasker {

	# Retrieval of parameters
	my ($executableFullPath, $numberOfThread, $rmlibrary, $useQuickSearch, $inputFastaFileFullPath, $keepTmpDir) = @_;

	# Initializations
	my $repeatMaskerFolder = 'RepeatMasker_tmp_dir';
	my $inputFileBasename = basename($inputFastaFileFullPath);
	my ($stdoutFile, $stderrFile, $mainOutputFile) = ($inputFileBasename . '.stdout', $inputFileBasename . '.stderr', $inputFileBasename . '.out');

	# Create RepeatMasker directory
	rename($repeatMaskerFolder, $repeatMaskerFolder . '.old.' . time()) if (-d $repeatMaskerFolder);
	mkdir($repeatMaskerFolder);

	# Build command line
	my $repeatMaskerCommand = $executableFullPath . ' -xm -nolow -engine crossmatch';
	$repeatMaskerCommand .= ' -dir ' . $repeatMaskerFolder;
	$repeatMaskerCommand .= ' -pa ' . $numberOfThread;
	$repeatMaskerCommand .= ' -lib ' . $rmlibrary;
	$repeatMaskerCommand .= ' -q' if ($useQuickSearch eq 'yes');
	$repeatMaskerCommand .= ' ' . $inputFastaFileFullPath;
	$repeatMaskerCommand .= ' 1> ' . $stdoutFile;
	$repeatMaskerCommand .= ' 2> ' . $stderrFile;

	# Execute RepeatMasker
	$mainLogger->debug('RepeatMasker will be executed with the following command line: ' . $repeatMaskerCommand);
	system ($repeatMaskerCommand);

	# Create a symlink to the main output file in the parent directory
	move($repeatMaskerFolder . '/' . $mainOutputFile, 'RepeatMasker_results.out');
	move($repeatMaskerFolder . '/' . $mainOutputFile . '.xm', 'RepeatMasker_results.xm');

	rmdir($repeatMaskerFolder) if (!$keepTmpDir);

	return 0; # SUCCESS
}


sub parseRepeatMasker {

	# Retrieval of parameters
	my ($teConfiguration, $rmResultFile, $rmConfiguration, $rmlibraryIndex) = @_;

	# Initializations
	my $validRepeatCounter = 0;
	my %repeatMaskerResults = ();

	# Display a warning message when the file to parse is missing
	if (! -e $rmResultFile) {
		$mainLogger->warn('Warning: RepeatMasker output file is missing (' . $rmResultFile . '). This parsing method will return an empty feature array.');
		return \%repeatMaskerResults;
	}

	# Parse the Result file (Note: Bio::Tools::RepeatMasker parser cannot be used because it does not give access to all the fields of the output file)
	open (REPLIST, '<' . $rmResultFile) or $mainLogger->logdie('Error: Cannot open/read file: ' . $rmResultFile);

	while ( my $repeatLine = <REPLIST> ) {
		# Jump over comment line
		$repeatLine !~ /\d+/ and next;

		# Initializations
		my %repeatData = ();

		# Split the line by columns
		my @columns = split (' ', $repeatLine);
		($repeatData{'score'}, $repeatData{'queryName'}, $repeatData{'queryStart'}, $repeatData{'queryEnd'}, $repeatData{'queryUnmasked'}) = @columns[0, 4, 5, 6, 7];
		($repeatData{'strand'}, $repeatData{'repeatName'}, $repeatData{'repeatClass'}, $repeatData{'hitEnd'}) = @columns[8, 9, 10, 12];

		# Skip the result when the repeated sequence is too small
		($repeatData{'queryEnd'} - $repeatData{'queryStart'}) < 30 and next;

		# Skip Simple repeats and Low complexity
		next if ($repeatData{'repeatClass'} =~ /^(Simple|low)$/i);

		# Clean the query name
		$repeatData{'queryName'} =~ s/[^\w#\.\-]/_/g;

		# Change the format of the queryUnmasked field
		$repeatData{'queryUnmasked'} =~ s/\(/-/g;
		$repeatData{'queryUnmasked'} =~ s/\)//g;

		# Get hit data depending on the strand
		$repeatData{'strand'} = ($repeatData{'strand'} eq '+') ? 1 : -1;
		$repeatData{'hitStart'} = ($repeatData{'strand'} == 1) ? $columns[11] : $columns[13];
		$repeatData{'hitUnmasked'} = ($repeatData{'strand'} == 1) ? $columns[13] : $columns[11];

		# Change the format of the hitUnmasked field
		$repeatData{'hitUnmasked'} =~ s/\(|\)//g;
		$repeatData{'hitUnmasked'} =~ s/\-//g;

		# This is used to know if the match corresponds to at least one of the extremity of the element
		$repeatData{'startAtExtremity'} = ($repeatData{'hitStart'} <= $rmConfiguration->{'extremityAreaLength'}) ? 'yes' : 'no';
		$repeatData{'endAtExtremity'} = ($repeatData{'hitUnmasked'} <= $rmConfiguration->{'extremityAreaLength'}) ? 'yes' : 'no';

		# Get short/long description
		($repeatData{'originalDescription'}, $repeatData{'shortDescription'}) = getDescriptions($rmlibraryIndex, $repeatData{'repeatName'}, $teConfiguration);

		# Increase counter of valid repeat_region
		$validRepeatCounter++;

		# Create & Store the RepeatMasker feature
		$repeatMaskerResults{$repeatData{'queryName'}} = [] if (! defined($repeatMaskerResults{$repeatData{'queryName'}}));
		push(@{$repeatMaskerResults{$repeatData{'queryName'}}}, Feature_builders::buildRepeatRegionFeature(\%repeatData, $validRepeatCounter));
	}

	close (REPLIST);

	return (\%repeatMaskerResults);
}


sub getDescriptions {

	# Retrieval of parameters
	my ($rmlibraryIndex, $repeatName, $teConfiguration) = @_;

	# Initializations
	my $originalDescription = $rmlibraryIndex->{$repeatName};
	$originalDescription or $originalDescription = 'No_description';
	my $shortDescription = 'Unknown';

	# Clean original description
	$originalDescription =~ s/[^\w\-\(\)\|,]/ /g;
	$originalDescription =~ tr/ //s;

	# Check the repeat identifier for a 3 letter tag (from the Wicker classification)
	if ($repeatName =~ /^([A-Z]{3})_/ig) {
		if (defined($teConfiguration->{'TE_classification'}->{$1})) {
			return ($originalDescription, $teConfiguration->{'TE_classification'}->{$1});
		}
	} else {
		# Check for keywords in the long description if the short description is still unknown
		foreach (my $i = 0; $i < scalar(keys %{$teConfiguration->{'Keywords_Order'}}); $i++) {
			my $categoryName = $teConfiguration->{'Keywords_Order'}->{$i};
			foreach my $key (sort keys (%{$teConfiguration->{'TE_keywords'}->{$categoryName}})) {
				if ($originalDescription =~ /$key/ig) {
					return ($originalDescription, $teConfiguration->{'TE_keywords'}->{$categoryName}->{$key});
				}
			}
		}
	}

	return ($originalDescription, $shortDescription);
}


sub getCloseRepeatsData {

	# Retrieval of parameters
	my ($repeatMaskerResults, $queryName, $index) = @_;

	# Initializations
	my %closeRepeatsData = ();

	$mainLogger->info("\t" . '-> Collecting position of the previous and next repeats..');

	# Is there a previous repeat ?
	if (defined($repeatMaskerResults->{$queryName}->[$index - 1]) && $index != 0) {
		$closeRepeatsData{'previousStart'} = $repeatMaskerResults->{$queryName}->[$index - 1]->start();
		$closeRepeatsData{'previousEnd'} = $repeatMaskerResults->{$queryName}->[$index - 1]->end();
	}

	# Is there a next repeat ?
	if (defined($repeatMaskerResults->{$queryName}->[$index + 1])) {
		$closeRepeatsData{'nextStart'} = $repeatMaskerResults->{$queryName}->[$index + 1]->start();
		$closeRepeatsData{'nextEnd'} = $repeatMaskerResults->{$queryName}->[$index + 1]->end();
	}

	$mainLogger->debug('Data collected for previous and next repeat:');
	$mainLogger->debug(Dumper(\%closeRepeatsData));

	return \%closeRepeatsData;
}

1;
