#!/usr/bin/env perl

package Writers;

# Basic Perl modules
use strict;
use warnings;
use diagnostics;

# Perl modules
use File::Basename;
use Log::Log4perl qw(get_logger);
use Clone qw(clone);

# BioPerl modules
use Bio::SeqIO;
use Bio::SeqFeature::Generic;
use Bio::Tools::GFF;

# Custom modules
use Misc;

# Get log4Perl logger
my $mainLogger = get_logger('');


sub writeAll {

	# Retrieval of parameters
	my ($filePrefix, $sequenceObject, $seqFeaturesArrayRef, $featureType, $csvFieldDelimiter, $programVersion) = @_;

	# Write EMBL
	writeEMBL($filePrefix . '.embl', $sequenceObject->seq(), $seqFeaturesArrayRef, $featureType);

	# Write GFF3 - Must be written after the others because it removes the global Note tag of features before writting them
	writeGFF($filePrefix . '.gff', $sequenceObject, $seqFeaturesArrayRef, $featureType, $programVersion);

	# Write Tabular
	writeTabular($filePrefix . '.csv', $seqFeaturesArrayRef, $featureType, $csvFieldDelimiter);

	return 0; # SUCCESS
}


sub writeEMBL {

	# Retrieval of parameters
	my ($emblFileFullPath, $sequenceAsString, $seqFeaturesArrayRef, $featureType) = @_;

	# Log
	$mainLogger->info('');
	$mainLogger->info('     -> Writing ' . $featureType . ' features in file ' . basename($emblFileFullPath));

	# Create a Bio::Seq object with all feature
	my $emblSeqObject = Bio::Seq->new( '-format' => 'EMBL', '-id' => substr($emblFileFullPath, 0, 10), '-seq' => $sequenceAsString );

	foreach my $currentFeature (@{$seqFeaturesArrayRef}) {
		# Initialization
		my $tmpFeatureToWrite = clone($currentFeature); # We have to duplicate the data to conserve the original feature tags for future use

		# Split the special Target tag into multiple note tag values
		if ($tmpFeatureToWrite->has_tag('Target')) {
			my ($repeatName, $repeatStart, $repeatEnd) = $tmpFeatureToWrite->get_tag_values('Target');
			$tmpFeatureToWrite->add_tag_value('Note', 'Target_name: ' . $repeatName);
			$tmpFeatureToWrite->add_tag_value('Note', 'Target_range: ' . $repeatStart . '..' . $repeatEnd);

			# Remove the original Target before writting
			$tmpFeatureToWrite->remove_tag('Target');
		}

		# Join the multiple values of the Junction_position_on_PCR_product into one string
		if ($tmpFeatureToWrite->has_tag('Junction_position_on_PCR_product')) {
			my @JunctionPositions = $tmpFeatureToWrite->get_tag_values('Junction_position_on_PCR_product');
			$tmpFeatureToWrite->remove_tag('Junction_position_on_PCR_product');
			$tmpFeatureToWrite->add_tag_value('Junction_position_on_PCR_product', join(',', @JunctionPositions));
		}

		# Join the multiple values of the Junction_position_on_query_sequence into one string
		if ($tmpFeatureToWrite->has_tag('Junction_position_on_query_sequence')) {
			my @JunctionPositions = $tmpFeatureToWrite->get_tag_values('Junction_position_on_query_sequence');
			$tmpFeatureToWrite->remove_tag('Junction_position_on_query_sequence');
			$tmpFeatureToWrite->add_tag_value('Junction_position_on_query_sequence', join(',', @JunctionPositions));
		}

		# Remove the Amplicon_sequence tag that is not needed for the EMBL output
		$tmpFeatureToWrite->remove_tag('Amplicon_sequence') if $tmpFeatureToWrite->has_tag('Amplicon_sequence');

		# Write the modified/adapted feature
		if ($featureType =~ /amplicon/ && $currentFeature->has_tag('Confidence_index')) {
			$mainLogger->info("\t" . '-> Amplicon "' . $currentFeature->display_name() . '" added to the EMBL file (Confidence index: ' . join(',', $currentFeature->get_tag_values('Confidence_index')) . ')');
		}
		$emblSeqObject->add_SeqFeature($tmpFeatureToWrite);
	}

	# Create a Bio::SeqIO objects for writting purpose
	my $emblSeqioObject = Bio::SeqIO->new( '-file' => '>' . $emblFileFullPath, '-format' => 'EMBL' );

	# Really write EMBL file
	$emblSeqioObject->write_seq($emblSeqObject);

	return 0; # SUCCESS
}


sub writeTabular {

	# Retrieval of parameters
	my ($tabularFileFullPath, $seqFeaturesArrayRef, $featureType, $csvFieldDelimiter) = @_;

	# Log
	$mainLogger->info('');
	$mainLogger->info('     -> Writing ' . $featureType . ' features in file ' . basename($tabularFileFullPath));

	# Create file
	open(TAB, '>' . $tabularFileFullPath) or $mainLogger->logdie('Error: Cannot create/open file: ' . $tabularFileFullPath);

	# Header
	my $headerLine = join($csvFieldDelimiter, ('#' , 'Amplicon_name', 'F_primer', 'F_primer_Tm', 'F_primer_pos', 'R_primer', 'R_primer_Tm', 'R_primer_pos', 'amplicon_seq', 'amplicon_size', 'amplicon_GC%', 'junction_type', 'junction_position_on_query_seq', 'junction_position_on_pcr_product', 'left_element', 'extremity_left', 'right_element', 'extremity_right', 'Confidence_level')) . "\n";
	print TAB $headerLine;

	# Write features
	foreach my $currentAmpliconFeature (@{$seqFeaturesArrayRef}) {
		# Initializations
		my $ampliconData = Misc::getHashFromNote($currentAmpliconFeature);

		# Some variable initializations for easier syntax
		my $ampliconID = join(',', $currentAmpliconFeature->get_tag_values('ID'));
		my $ampliconSequence = ($currentAmpliconFeature->has_tag('Amplicon_sequence')) ? join(',', $currentAmpliconFeature->get_tag_values('Amplicon_sequence')) : 'Missing_sequence';
		my $junctionType = join(',', $currentAmpliconFeature->get_tag_values('Junction_type'));
		my $junctionPositionOnQuerySeq = join(',', $currentAmpliconFeature->get_tag_values('Junction_position_on_query_sequence'));
		my $junctionPositionOnPcrProduct = join(',', $currentAmpliconFeature->get_tag_values('Junction_position_on_PCR_product'));
		my $leftElement = ($currentAmpliconFeature->has_tag('Left_element')) ? join(',', $currentAmpliconFeature->get_tag_values('Left_element')) : '.';
		my $leftExtremity = ($currentAmpliconFeature->has_tag('Left_extremity')) ? join(',', $currentAmpliconFeature->get_tag_values('Left_extremity')) : '.';
		my $rightElement = ($currentAmpliconFeature->has_tag('Right_element')) ? join(',', $currentAmpliconFeature->get_tag_values('Right_element')) : '.';
		my $rightExtremity = ($currentAmpliconFeature->has_tag('Right_extremity')) ? join(',', $currentAmpliconFeature->get_tag_values('Right_extremity')) : '.';
		my $computedConfidenceIndex = ($currentAmpliconFeature->has_tag('Confidence_index')) ? join(',', $currentAmpliconFeature->get_tag_values('Confidence_index')) : 'none';

		# Build the informative line
		my $lineToPrint = join($csvFieldDelimiter, ($ampliconID, $ampliconData->{'Forward_primer'}, $ampliconData->{'Forward_TM'}, $currentAmpliconFeature->start(),
						$ampliconData->{'Reverse_primer'}, $ampliconData->{'Reverse_TM'}, $currentAmpliconFeature->end(), $ampliconSequence, $ampliconData->{'Amplicon_size'},
						$ampliconData->{'Amplicon_GC_percent'}, $junctionType, $junctionPositionOnQuerySeq, $junctionPositionOnPcrProduct, $leftElement, $leftExtremity,
						$rightElement, $rightExtremity, $computedConfidenceIndex)) . "\n";

		# Print the line in the main tabular file in every case
		if ($featureType =~ /amplicon/ && $currentAmpliconFeature->has_tag('Confidence_index')) {
			$mainLogger->info("\t" . '-> Amplicon "' . $currentAmpliconFeature->display_name() . '" added to the CSV file (Confidence index: ' . join(',', $currentAmpliconFeature->get_tag_values('Confidence_index')) . ')');
		}
		print TAB $lineToPrint;
	}

	close(TAB);

	return 0; # SUCCESS
}


sub writeGFF {

	# Retrieval of parameters
	my ($gffFileFullPath, $sequenceObject, $seqFeaturesArrayRef, $featureType, $programVersion) = @_;

	# Log
	$mainLogger->info('');
	$mainLogger->info('     -> Writing ' . $featureType . ' features in file ' . basename($gffFileFullPath));

	# Creation of a new GFF writer object
	my $gffWriterObject = Bio::Tools::GFF->new(-fh => \*GFF3, -gff_version => 3);

	# Creation of the first line of the GFF
	my $Origin = buildGFF3FirstLine($sequenceObject, $programVersion);

	# Write all features into the GFF3 file
	open(GFF3, '>' . $gffFileFullPath) or $mainLogger->logdie('Error: Cannot create/open file: ' . $gffFileFullPath);

	$gffWriterObject->write_feature($Origin);

	foreach my $currentFeature (@{$seqFeaturesArrayRef}) {
		# Initialization
		my $tmpFeatureToWrite = clone($currentFeature); # We have to duplicate the data to conserve the original feature tags for future use

		# Split the original multi-note field/tag into individual tags
		if ($tmpFeatureToWrite->has_tag('Note')) {
			foreach my $noteCouple ($tmpFeatureToWrite->get_tag_values('Note')) {
				my ($key, $value) = split(': ', $noteCouple);
				$tmpFeatureToWrite->add_tag_value(lc($key), $value);
			}
			# Remove the original full Note field before writting
			$tmpFeatureToWrite->remove_tag('Note');
		}

		# Remove the Amplicon_sequence tag that is not needed for the EMBL output
		$tmpFeatureToWrite->remove_tag('Amplicon_sequence') if $tmpFeatureToWrite->has_tag('Amplicon_sequence');

		# Write the modified/adapted feature
		if ($featureType =~ /amplicon/ && $tmpFeatureToWrite->has_tag('Confidence_index')) {
			$mainLogger->info("\t" . '-> Amplicon "' . $tmpFeatureToWrite->display_name() . '" added to the GFF file (Confidence index: ' . join(',', $tmpFeatureToWrite->get_tag_values('Confidence_index')) . ')');
		}
		$gffWriterObject->write_feature($tmpFeatureToWrite);
	}

	close(GFF3);

	return 0; # SUCCESS
}


sub buildGFF3FirstLine {

	# Retrieval of parameters
	my ($sequenceObject, $programVersion) = @_;

	# Creation of the first basic feature
	my $Origin_attribute = {};

	# Define the basic feature attributes
	$Origin_attribute->{'ID'} = $sequenceObject->display_id();

	# Define custom attributes
	$Origin_attribute->{'isbpFinder_version'} = $programVersion;
	$Origin_attribute->{'Ontology_term'} = 'SO:0000001';

	# Create the feature itself
	my $Origin = Bio::SeqFeature::Generic->new(
						 -seq_id      => $Origin_attribute->{'ID'},
						 -source_tag  => 'isbpFinder',
						 -primary_tag => 'region',
						 -start       => 1,
						 -end         =>  $sequenceObject->length(),
						 -tag         => $Origin_attribute
						);

	return $Origin;
}

1;
